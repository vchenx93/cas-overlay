<?xml version="1.0" encoding="UTF-8"?>
<project
  xmlns="http://maven.apache.org/POM/4.0.0"
  xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
  xsi:schemaLocation="
    http://maven.apache.org/POM/4.0.0 http://maven.apache.org/maven-v4_0_0.xsd">

  <modelVersion>4.0.0</modelVersion>
  <groupId>edu.vt.middleware</groupId>
  <artifactId>cas-overlay</artifactId>
  <version>3.5.2.5</version>
  <packaging>war</packaging>
  <name>VT CAS Server Web Application</name>
  <url>http://middleware.vt.edu/doku.php?id=middleware:cas</url>

  <repositories>
    <repository>
      <id>middleware</id>
      <name>Middleware Repository</name>
      <url>https://git.it.vt.edu/middleware/maven-repo/raw/master</url>
    </repository>
    <repository>
      <id>vt-middleware</id>
      <url>https://raw.github.com/vt-middleware/maven-repo/master</url>
      <snapshots/>
    </repository>
  </repositories>

  <distributionManagement>
    <repository>
      <id>middleware</id>
      <url>dav:https://www.middleware.vt.edu/pubs-webdav/maven2/</url>
      <name>Middleware Maven2 Repository</name>
    </repository>
  </distributionManagement>

  <dependencies>
    <!-- Compile dependencies -->
    <dependency>
      <groupId>org.jasig.cas</groupId>
      <artifactId>cas-server-core</artifactId>
      <version>${cas.version}</version>
      <exclusions>
        <exclusion>
          <groupId>org.bouncycastle</groupId>
          <artifactId>bcprov-jdk15</artifactId>
        </exclusion>
      </exclusions>
    </dependency>
    <dependency>
      <groupId>org.jasig.cas</groupId>
      <artifactId>cas-server-support-x509</artifactId>
      <version>${cas.version}</version>
      <exclusions>
        <exclusion>
          <groupId>org.bouncycastle</groupId>
          <artifactId>bcprov-jdk15</artifactId>
        </exclusion>
      </exclusions>
    </dependency>
    <dependency>
      <groupId>org.jasig.cas</groupId>
      <artifactId>cas-server-support-ldap</artifactId>
      <version>${cas.version}</version>
    </dependency>
    <dependency>
      <groupId>edu.vt.middleware</groupId>
      <artifactId>vt-ldap</artifactId>
      <version>3.0</version>
    </dependency>
    <dependency>
      <groupId>com.github.inspektr</groupId>
      <artifactId>inspektr-audit</artifactId>
      <version>${inspektr.version}</version>
    </dependency>
    <dependency>
      <groupId>net.sf.ehcache</groupId>
      <artifactId>ehcache-core</artifactId>
      <version>2.1.0</version>
    </dependency>

    <!-- Provided dependencies -->
    <dependency>
      <groupId>javax.servlet</groupId>
      <artifactId>servlet-api</artifactId>
      <version>2.5</version>
      <scope>provided</scope>
    </dependency>
    <dependency>
      <groupId>javax.servlet.jsp</groupId>
      <artifactId>javax.servlet.jsp-api</artifactId>
      <version>2.2.1</version>
      <scope>provided</scope>
    </dependency>
    <dependency>
      <groupId>org.apache.tomcat</groupId>
      <artifactId>tomcat-jdbc</artifactId>
      <version>7.0.29</version>
      <scope>provided</scope>
    </dependency>

    <!-- Runtime dependencies -->
    <dependency>
      <groupId>org.jasig.cas</groupId>
      <artifactId>cas-server-webapp</artifactId>
      <version>${cas.version}</version>
      <type>war</type>
      <scope>runtime</scope>
    </dependency>
    <dependency>
      <groupId>org.jasig.cas</groupId>
      <artifactId>cas-server-integration-restlet</artifactId>
      <version>${cas.version}</version>
      <scope>runtime</scope>
    </dependency>
    <dependency>
      <groupId>org.jasig.cas</groupId>
      <artifactId>cas-server-integration-memcached</artifactId>
      <version>${cas.version}</version>
      <scope>runtime</scope>
    </dependency>
    <dependency>
      <groupId>com.sghe.cas</groupId>
      <artifactId>sghe-cas-extensions</artifactId>
      <version>3.4.6</version>
      <scope>runtime</scope>
    </dependency>
    <dependency>
      <groupId>org.springframework.security</groupId>
      <artifactId>spring-security-ldap</artifactId>
      <version>${spring.security.version}</version>
      <scope>runtime</scope>
    </dependency>
    <dependency>
      <groupId>org.hibernate</groupId>
      <artifactId>hibernate-entitymanager</artifactId>
      <version>${hibernate.version}</version>
      <scope>runtime</scope>
    </dependency>
    <dependency>
      <groupId>org.hibernate</groupId>
      <artifactId>hibernate-validator</artifactId>
      <version>${hibernate.validator.version}</version>
      <scope>runtime</scope>
    </dependency>
    <dependency>
      <groupId>commons-pool</groupId>
      <artifactId>commons-pool</artifactId>
      <version>1.6</version>
      <scope>runtime</scope>
    </dependency>
    <dependency>
      <groupId>edu.vt.middleware</groupId>
      <artifactId>vt-servlet-filters</artifactId>
      <version>2.0.1</version>
      <scope>runtime</scope>
    </dependency>
    <dependency>
      <groupId>org.codehaus.jackson</groupId>
      <artifactId>jackson-mapper-asl</artifactId>
      <version>1.7.1</version>
    </dependency>
    <!-- log4j extras provides EnhancedPatternLayout -->
    <dependency>
      <groupId>log4j</groupId>
      <artifactId>apache-log4j-extras</artifactId>
      <version>1.1</version>
    </dependency>
    <dependency>
      <groupId>edu.vt.middleware</groupId>
      <artifactId>spring-webflow-client-repo</artifactId>
      <version>1.0-RC1</version>
    </dependency>

    <!-- Test dependencies -->
    <dependency>
      <groupId>junit</groupId>
      <artifactId>junit</artifactId>
      <version>4.8.2</version>
      <scope>test</scope>
    </dependency>
    <dependency>
      <groupId>org.springframework</groupId>
      <artifactId>spring-test</artifactId>
      <version>${spring.version}</version>
      <scope>test</scope>
    </dependency>
    <dependency>
      <groupId>commons-collections</groupId>
      <artifactId>commons-collections</artifactId>
      <version>3.2.1</version>
      <scope>test</scope>
    </dependency>
    <dependency>
      <groupId>org.opensymphony.quartz</groupId>
      <artifactId>quartz</artifactId>
      <version>1.6.1</version>
      <scope>test</scope>
    </dependency>
    <dependency>
      <groupId>xml-apis</groupId>
      <artifactId>xml-apis</artifactId>
      <version>1.4.01</version>
      <scope>test</scope>
    </dependency>
  </dependencies>

  <build>
    <finalName>cas</finalName>
    <plugins>
      <plugin>
        <groupId>org.apache.maven.plugins</groupId>
        <artifactId>maven-enforcer-plugin</artifactId>
        <version>1.1</version>
        <executions>
          <execution>
            <id>enforce-files</id>
            <goals>
              <goal>enforce</goal>
            </goals>
            <configuration>
              <rules>
                <requireFilesExist>
                  <files>
                    <file>${basedir}/src/test/resources/edid.keystore</file>
                  </files>
                </requireFilesExist>
              </rules>
              <fail>true</fail>
            </configuration>
          </execution>
        </executions>
      </plugin>

      <plugin>
        <groupId>org.apache.maven.plugins</groupId>
        <artifactId>maven-compiler-plugin</artifactId>
        <version>2.5.1</version>
        <configuration>
          <source>1.5</source>
          <target>1.5</target>
        </configuration>
      </plugin>

      <plugin>
        <groupId>org.apache.maven.plugins</groupId>
        <artifactId>maven-checkstyle-plugin</artifactId>
        <version>2.9.1</version>
        <configuration>
          <configLocation>${checkstyle.dir}/checkstyle.xml</configLocation>
          <headerLocation>${checkstyle.dir}/checkstyle_header</headerLocation>
          <suppressionsLocation>${checkstyle.dir}/suppressions.xml</suppressionsLocation>
          <failsOnError>true</failsOnError>
        </configuration>
        <executions>
          <execution>
            <id>checkstyle</id>
            <phase>compile</phase>
            <goals>
              <goal>checkstyle</goal>
            </goals>
          </execution>
        </executions>
      </plugin>

      <plugin>
        <groupId>org.apache.maven.plugins</groupId>
        <artifactId>maven-war-plugin</artifactId>
        <version>2.2</version>
        <configuration>
          <overlays>
            <overlay>
              <groupId>org.jasig.cas</groupId>
              <artifactId>cas-server-webapp</artifactId>
              <excludes>
                <exclude>css/**</exclude>
                <exclude>favicon.ico</exclude>
                <exclude>js/**</exclude>
                <exclude>images/*.gif</exclude>
                <exclude>themes/**</exclude>
                <exclude>WEB-INF/cas.properties</exclude>
                <exclude>WEB-INF/classes/default_views.properties</exclude>
                <exclude>WEB-INF/classes/log4j.xml</exclude>
                <exclude>WEB-INF/classes/messages*.properties</exclude>
                <exclude>WEB-INF/lib/bcprov-*</exclude>
                <exclude>WEB-INF/lib/javassist-*</exclude>
                <exclude>WEB-INF/lib/javassist-*</exclude>
                <exclude>WEB-INF/lib/jaxb-impl-*</exclude>
                <exclude>WEB-INF/lib/joda-time-1*</exclude>
                <exclude>WEB-INF/lib/log4j-*</exclude>
                <exclude>WEB-INF/view/jsp/default/**</exclude>
                <exclude>WEB-INF/view/jsp/services/**</exclude>
              </excludes>
            </overlay>
          </overlays>
        </configuration>
      </plugin>

      <!-- Build and install a JAR artifact for when we need to depend on VT source -->
      <plugin>
        <groupId>org.apache.maven.plugins</groupId>
        <artifactId>maven-jar-plugin</artifactId>
        <version>2.4</version>
        <configuration>
          <finalName>${project.artifactId}-${project.version}</finalName>
        </configuration>
        <executions>
          <execution>
            <phase>package</phase>
            <goals>
              <goal>jar</goal>
            </goals>
          </execution>
        </executions>
      </plugin>
      <plugin>
        <groupId>org.apache.maven.plugins</groupId>
        <artifactId>maven-install-plugin</artifactId>
        <executions>
          <execution>
            <phase>install</phase>
            <goals>
              <goal>install-file</goal>
            </goals>
            <configuration>
              <packaging>jar</packaging>
              <artifactId>${project.artifactId}</artifactId>
              <groupId>${project.groupId}</groupId>
              <version>${project.version}</version>
              <file>
                ${project.build.directory}/${project.artifactId}-${project.version}.jar
              </file>
            </configuration>
          </execution>
        </executions>
      </plugin>

    </plugins>
  </build>

  <properties>
    <checkstyle.dir>${basedir}/src/checkstyle</checkstyle.dir>

    <!-- Version properties -->
    <cas.version>3.5.2.1</cas.version>
    <hibernate.version>4.1.0.Final</hibernate.version>
    <hibernate.validator.version>4.2.0.Final</hibernate.validator.version>
    <inspektr.version>1.0.7.GA</inspektr.version>
    <spring.version>3.1.1.RELEASE</spring.version>
    <spring.security.version>3.1.0.RELEASE</spring.security.version>
  </properties>
</project>
