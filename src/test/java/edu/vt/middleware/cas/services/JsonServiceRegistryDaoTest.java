/*
  $Id$

  Copyright (C) 2013 Virginia Tech.
  All rights reserved.

  SEE LICENSE FOR MORE INFORMATION

  Author:  Middleware
  Email:   middleware@vt.edu
  Version: $Revision$
  Updated: $Date$
*/
package edu.vt.middleware.cas.services;

import java.util.Arrays;

import org.jasig.cas.services.RegisteredService;
import org.jasig.cas.services.RegisteredServiceImpl;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

/**
 * Unit test for {@link JsonServiceRegistryDao}.
 *
 * @author  Middleware Services
 * @version  $Revision$
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration({
        "classpath:/applicationContext-test.xml",
        "file:src/main/webapp/WEB-INF/deployerConfigContext.xml"
})
public class JsonServiceRegistryDaoTest {

    @Autowired
    private JsonServiceRegistryDao serviceRegistryDao;

    @Before
    @After
    public void cleanUp() throws Exception {
        for (RegisteredService service : serviceRegistryDao.load()) {
            serviceRegistryDao.makeFile(service).delete();
        }
    }

    @Test
    public void testSaveFindDelete() throws Exception {
        final String serviceId = "https://nobody.vt.edu/**";
        final RegisteredService saved = serviceRegistryDao.save(createService("Test1", serviceId));
        Assert.assertNotNull(saved);
        Assert.assertTrue(saved.getId() > 0);
        Assert.assertEquals(serviceId, saved.getServiceId());
        Assert.assertTrue(serviceRegistryDao.makeFile(saved).exists());
        final RegisteredService found = serviceRegistryDao.findServiceById(saved.getId());
        Assert.assertNotNull(found);
        Assert.assertEquals(found.getId(), saved.getId());
        Assert.assertEquals(found.getServiceId(), saved.getServiceId());
        Assert.assertTrue(serviceRegistryDao.delete(found));
        Assert.assertFalse(serviceRegistryDao.makeFile(saved).exists());
    }

    @Test
    public void testLoad() throws Exception {
        serviceRegistryDao.save(createService("Load1", "https://a.vt.edu/**"));
        serviceRegistryDao.save(createService("Load2", "https://b.vt.edu/**"));
        serviceRegistryDao.save(createService("Load3", "https://c.vt.edu/**"));
        Assert.assertEquals(3, serviceRegistryDao.load().size());
    }

    private static RegisteredService createService(final String name, final String serviceId) {
        final RegisteredServiceImpl service = new RegisteredServiceImpl();
        service.setName(name);
        service.setServiceId(serviceId);
        service.setAllowedToProxy(false);
        service.setAllowedAttributes(Arrays.asList("authId", "virginiaTechAffiliation", "uid", "groupMembership"));
        service.setAnonymousAccess(false);
        service.setDescription("Service created by JsonServiceRegistryDao unit test.");
        service.setEnabled(true);
        service.setEvaluationOrder(100);
        service.setSsoEnabled(true);
        return service;
    }
}
