/*
  $Id$

  Copyright (C) 2008 Virginia Tech, Marvin S. Addison.
  All rights reserved.

  SEE LICENSE FOR MORE INFORMATION

  Author:  Marvin S. Addison
  Email:   serac@vt.edu
  Version: $Revision$
  Updated: $Date$
*/
package edu.vt.middleware.cas;

import java.io.File;
import java.io.FilenameFilter;

/**
 * File name filter implementation that selects for files that have a
 * particular file name suffix, e.g. .html.
 *
 * @author Marvin S. Addison
 * @version $Revision$
 *
 */
public class SuffixFileNameFilter implements FilenameFilter {

    /** File suffix including leading dot */
    private String suffix;

    /**
     * Creates a new instance that selects for file names ending with the
     * given suffix.
     * @param suffix File name suffix.
     */
    public SuffixFileNameFilter(final String suffix) {
        if (suffix.startsWith(".")) {
            this.suffix = suffix;
        } else {
            this.suffix = "." + suffix;
        }
    }

    /** {@inheritDoc} */
    public boolean accept(final File dir, final String name) {
        return name.endsWith(suffix);
    }

}
