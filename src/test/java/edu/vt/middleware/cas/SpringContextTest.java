/**
 * Licensed to Jasig under one or more contributor license
 * agreements. See the NOTICE file distributed with this work
 * for additional information regarding copyright ownership.
 * Jasig licenses this file to you under the Apache License,
 * Version 2.0 (the "License"); you may not use this file
 * except in compliance with the License. You may obtain a
 * copy of the License at:
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package edu.vt.middleware.cas;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.jasig.cas.adaptors.ldap.BindLdapAuthenticationHandler;
import org.jasig.cas.authentication.principal.UsernamePasswordCredentials;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.mock.jndi.SimpleNamingContextBuilder;
import org.springframework.test.annotation.IfProfileValue;
import org.springframework.test.annotation.Repeat;
import org.springframework.test.annotation.Timed;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

/**
 * Validates various aspects of Spring context configuration.
 *
 * @author Marvin S. Addison
 * @version $Revision$
 *
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration({
        "classpath:/applicationContext-test.xml",
        "file:src/main/webapp/WEB-INF/deployerConfigContext.xml"
})
public class SpringContextTest {

    /** Logger instance. */
    private final Log logger = LogFactory.getLog(getClass());

    @Autowired
    private ApplicationContext context;

    @Autowired
    private BindLdapAuthenticationHandler handler;

    @Before
    public void setup() throws Exception {
        // Deactivate the mock JNDI context before each test so it doesn't
        // interfere with LDAP initial context creation
        SimpleNamingContextBuilder builder = SimpleNamingContextBuilder.getCurrentContextBuilder();
        if (builder != null) {
            builder.deactivate();
        }
    }

    @Test
    public void testWiring() throws Exception {
        Assert.assertTrue(context.getBeanDefinitionCount() > 0);
    }

    /**
     * Optional LDAP authentication performance test.  The following system properties must be set:
     *
     * <ul>
     *     <li>profile=perf</li>
     *     <li>edauth.user=pid_to_authenticate_with</li>
     *     <li>edauth.pass=pid_password</li>
     * </ul>
     *
     * <p>For example:</p>
     *
     * <code>mvn -DargLine='-Dprofile=perf -Dedauth.user=pid -Dedauth.pass=foo' clean test</code>
     *
     * @throws Exception On test errors.
     */
    @Test
    @Repeat(500)
    @Timed(millis = 30000)
    @IfProfileValue(name = "profile", value = "perf")
    public void testLDAPAuthSpeed() throws Exception {
        handler.authenticate(newTestCredentials());
    }


    private UsernamePasswordCredentials newTestCredentials() {
        final String user = System.getProperty("edauth.user");
        if (user == null) {
            throw new RuntimeException("edauth.user system property not set");
        }
        final String pass = System.getProperty("edauth.pass");
        if (pass == null) {
            throw new RuntimeException("edauth.pass system property not set");
        }
        final UsernamePasswordCredentials creds = new UsernamePasswordCredentials();
        creds.setUsername(user);
        creds.setPassword(pass);
        return creds;
    }
}
