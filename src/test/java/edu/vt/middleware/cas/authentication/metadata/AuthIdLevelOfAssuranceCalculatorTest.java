/*
  $Id: $

  Copyright (C) 2009 Virginia Tech.
  All rights reserved.

  SEE LICENSE FOR MORE INFORMATION

  Author:  Middleware
  Email:   middleware@vt.edu
  Version: $Revision: $
  Updated: $Date: $
*/
package edu.vt.middleware.cas.authentication.metadata;

import java.util.ArrayList;
import java.util.List;

import org.jasig.cas.authentication.principal.Credentials;
import org.jasig.cas.authentication.principal.UsernamePasswordCredentials;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

/**
 * Unit test for {@link AuthIdLevelOfAssuranceCalculator} class.
 *
 * @author Middleware
 * @version $Revision: $
 *
 */
@RunWith(Parameterized.class)
public class AuthIdLevelOfAssuranceCalculatorTest {
    /** Subject of test */
    private final AuthIdLevelOfAssuranceCalculator calculator = new AuthIdLevelOfAssuranceCalculator();

    /** Test credential */
    private Credentials credentials;

    /** Expected LOA value */
    private String expectedLoa;


    /**
     * Creates a new test instance with the given parameters.
     *
     * @param cred Credential to test.
     * @param loa Expected LOA value based on credential.
     */
    public AuthIdLevelOfAssuranceCalculatorTest(
            final Credentials cred,
            final String loa) {
        credentials = cred;
        expectedLoa = loa;
        calculator.setGuestAccountLevel("guest");
        calculator.setPidAccountLevel("pid");
    }


    /**
     * Parameterized test data.
     * @return Test parameters.
     */
    @Parameters
    public static List<Object[]> getTestParms() {
        final List<Object[]> params = new ArrayList<Object[]>(6);
        final UsernamePasswordCredentials uupid =
            new UsernamePasswordCredentials();
        uupid.setUsername("serac");
        final UsernamePasswordCredentials guestId =
            new UsernamePasswordCredentials();
        guestId.setUsername("nobody@example.com");
        params.add(new Object[] {uupid, "pid"});
        params.add(new Object[] {guestId, "guest"});
        return params;
    }


    /**
     * Tests the {@link edu.vt.middleware.cas.authentication.metadata.AuthIdLevelOfAssuranceCalculator#calculate(Credentials)}
     * method.
     */
    @Test
    public void testPopulateAttributes() {
        Assert.assertEquals(expectedLoa, calculator.calculate(credentials));
    }
}
