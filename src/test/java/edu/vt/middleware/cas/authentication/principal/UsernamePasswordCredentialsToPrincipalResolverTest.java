/*
  $Id$

  Copyright (C) 2009 Virginia Tech.
  All rights reserved.

  SEE LICENSE FOR MORE INFORMATION

  Author:  Middleware
  Email:   middleware@vt.edu
  Version: $Revision$
  Updated: $Date$
*/
package edu.vt.middleware.cas.authentication.principal;

import java.util.ArrayList;
import java.util.Collection;

import org.jasig.cas.authentication.principal.Principal;
import org.jasig.cas.authentication.principal.SimplePrincipal;
import org.jasig.cas.authentication.principal.UsernamePasswordCredentials;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 * Unit test for {@link UsernamePasswordCredentialsToPrincipalResolver} class.
 *
 * @author Middleware
 * @version $Revision$
 *
 */
@RunWith(Parameterized.class)
public class UsernamePasswordCredentialsToPrincipalResolverTest {

    /** Test Spring context */
    private static final ApplicationContext CONTEXT =
        new ClassPathXmlApplicationContext("/resolver-test.xml");

    /** Test credentials */
    private final UsernamePasswordCredentials credentials;

    /** Expected principal to be resolved */
    private final Principal expected;

    /** Subject of test */
    private final UsernamePasswordCredentialsToPrincipalResolver resolver;


    /**
     * Creates a new test instance with the given parameters.
     *
     * @param  testCredentials  Test username/password credentials.
     * @param  expectedResult  Expected principal to be resolved.
     */
    public UsernamePasswordCredentialsToPrincipalResolverTest(
            final UsernamePasswordCredentials testCredentials,
            final Principal expectedResult) {

        resolver = (UsernamePasswordCredentialsToPrincipalResolver)
            CONTEXT.getBean("usernameCredentialsResolver");
        credentials = testCredentials;
        expected = expectedResult;
    }


    /**
     * Gets the unit test parameters.
     *
     * @return  Test parameter data.
     *
     * @throws  Exception on parameter setup errors.
     */
    @Parameters
    public static Collection<Object[]> getTestParameters() throws Exception {
        final Collection<Object[]> params = new ArrayList<Object[]>();

        // Test case #1
        final UsernamePasswordCredentials cred1 =
            new UsernamePasswordCredentials();
        cred1.setUsername("serac");
        params.add(new Object[] { cred1, new SimplePrincipal("serac") });

        // Test case #2 -- ensure normalization of mixed case
        final UsernamePasswordCredentials cred2 =
            new UsernamePasswordCredentials();
        cred2.setUsername("sEraC");
        params.add(new Object[] { cred2, new SimplePrincipal("serac") });

        return params;
    }


    /**
     * Test method for
     * {@link UsernamePasswordCredentialsToPrincipalResolver
     *#resolvePrincipal(Credentials)}.
     */
    @Test
    public void testResolvePrincipal() {
        Assert.assertEquals(
                expected,
                resolver.resolvePrincipal(credentials));
    }

}
