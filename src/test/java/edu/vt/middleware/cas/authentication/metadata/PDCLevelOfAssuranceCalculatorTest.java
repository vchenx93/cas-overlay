/*
  $Id: $

  Copyright (C) 2012 Virginia Tech.
  All rights reserved.

  SEE LICENSE FOR MORE INFORMATION

  Author:  Middleware Services
  Email:   middleware@vt.edu
  Version: $Revision: $
  Updated: $Date: $
*/
package edu.vt.middleware.cas.authentication.metadata;

import java.security.cert.X509Certificate;
import java.util.ArrayList;
import java.util.Collection;
import java.util.LinkedHashMap;

import edu.vt.middleware.cas.util.X509Helper;
import org.jasig.cas.adaptors.x509.authentication.principal.X509CertificateCredentials;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

/**
 * Unit test for {@link edu.vt.middleware.cas.authentication.metadata.PDCLevelOfAssuranceCalculator} class.
 *
 * @author Middleware
 * @version $Revision: $
 *
 */
@RunWith(Parameterized.class)
public class PDCLevelOfAssuranceCalculatorTest {
   
    /** Subject of test. */
    private PDCLevelOfAssuranceCalculator calculator;
   
    /** Test certificate chain. */
    private X509Certificate[] certificates;

    /** Expected LOA. */
    private String expected;


    public PDCLevelOfAssuranceCalculatorTest(
            final PDCLevelOfAssuranceCalculator calc, final String certPath, final String expectedLoa) {
        
        this.calculator = calc;
        this.expected = expectedLoa;
        this.certificates = X509Helper.readCertificates(certPath);
    }


    /**
     * Gets the unit test parameters.
     *
     * @return  Test parameter data.
     *
     * @throws  Exception on parameter setup errors.
     */
    @Parameterized.Parameters
    public static Collection<Object[]> getTestParameters() throws Exception {
        final Collection<Object[]> params = new ArrayList<Object[]>();
        PDCLevelOfAssuranceCalculator calc;

        // Test case #1 -- cert containing matching OID
        calc = new PDCLevelOfAssuranceCalculator();
        calc.setDefaultLevel("default");
        calc.setOidLevelMap(new LinkedHashMap<String, String>() {{
            put("1.3.6.1.4.1.6760.5.2.2.5.1", "silver");
            put("1.3.6.1.4.1.6760.5.2.2.4.1", "bronze");
        }});
        params.add(new Object[] { calc, "/serac-dev-test-cert-chain.pem", "bronze" });

        // Test case #2 -- cert without matching OID
        calc = new PDCLevelOfAssuranceCalculator();
        calc.setDefaultLevel("default");
        calc.setOidLevelMap(new LinkedHashMap<String, String>() {{
            put("1.3.6.1.4.1.6760.5.2.2.6.1", "gold");
            put("1.3.6.1.4.1.6760.5.2.2.5.1", "silver");
        }});
        params.add(new Object[] { calc, "/serac-dev-test-cert-chain.pem", "default" });

        return params;
    }


    @Test
    public void testCalculate() throws Exception {
        Assert.assertEquals(expected, calculator.calculate(new X509CertificateCredentials(certificates)));
    }
}
