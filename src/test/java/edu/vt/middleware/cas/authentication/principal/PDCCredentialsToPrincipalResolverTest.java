/*
  $Id$

  Copyright (C) 2009 Virginia Tech.
  All rights reserved.

  SEE LICENSE FOR MORE INFORMATION

  Author:  Middleware
  Email:   middleware@vt.edu
  Version: $Revision$
  Updated: $Date$
*/
package edu.vt.middleware.cas.authentication.principal;

import java.util.ArrayList;
import java.util.Collection;

import edu.vt.middleware.cas.util.X509Helper;
import org.jasig.cas.adaptors.x509.authentication.principal.X509CertificateCredentials;
import org.jasig.cas.authentication.principal.Principal;
import org.jasig.cas.authentication.principal.SimplePrincipal;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 * Unit test for {@link PDCCredentialsToPrincipalResolver} class.
 *
 * @author Middleware
 * @version $Revision$
 *
 */
@RunWith(Parameterized.class)
public class PDCCredentialsToPrincipalResolverTest {

    /** Test Spring context */
    private static final ApplicationContext CONTEXT =
        new ClassPathXmlApplicationContext("/resolver-test.xml");

    /** Test credentials */
    private final X509CertificateCredentials credentials;

    /** Expected principal to be resolved */
    private final Principal expected;

    /** Subject of test */
    private final PDCCredentialsToPrincipalResolver resolver;


    /**
     * Creates a new test instance with the given parameters.
     *
     * @param  certPath Path to test certificate.
     * @param  expectedResult  Expected principal to be resolved.
     */
    public PDCCredentialsToPrincipalResolverTest(final String certPath, final Principal expectedResult) {

        resolver = CONTEXT.getBean("pdcCredentialsResolver", PDCCredentialsToPrincipalResolver.class);
        credentials = new X509CertificateCredentials(X509Helper.readCertificates(certPath));
        expected = expectedResult;
    }


    /**
     * Gets the unit test parameters.
     *
     * @return  Test parameter data.
     *
     * @throws  Exception on parameter setup errors.
     */
    @Parameters
    public static Collection<Object[]> getTestParameters() throws Exception {
        final Collection<Object[]> params = new ArrayList<Object[]>();

        // Test case #1 -- valid cert
        params.add(new Object[] { "/serac-dev-test-cert-chain.pem", new SimplePrincipal("serac") });

        // Test case #2 -- invalid cert contains null for certificate policies extension
        params.add(new Object[] { "/thawte-freemail-serac@vt.edu-cert.pem", null });

        return params;
    }


    /**
     * Test method for
     * {@link PDCCredentialsToPrincipalResolver#resolvePrincipal(Credentials)}.
     */
    @Test
    public void testResolvePrincipal() {
        Assert.assertEquals(expected, resolver.resolvePrincipal(credentials));
    }
}
