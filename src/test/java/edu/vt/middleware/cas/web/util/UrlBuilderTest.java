/*
  $Id: $

  Copyright (C) 2012 Virginia Tech.
  All rights reserved.

  SEE LICENSE FOR MORE INFORMATION

  Author:  Middleware Services
  Email:   middleware@vt.edu
  Version: $Revision: $
  Updated: $Date: $
*/
package edu.vt.middleware.cas.web.util;

import java.util.Arrays;
import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.webflow.core.collection.LocalParameterMap;
import org.springframework.webflow.execution.RequestContext;
import org.springframework.webflow.test.MockRequestContext;

/**
 * Unit test for {@link UrlBuilder} class.
 *
 * @author Middleware Services
 * @version: $Revision: $
 */
@RunWith(Parameterized.class)
public class UrlBuilderTest {
    private String baseUrl;
    private RequestContext mockRequest;
    private String expectedUrl;
    
    public UrlBuilderTest(
            final String url,
            final MockRequestContext request,
            final String expected)
    {
        this.baseUrl = url;
        this.mockRequest = request;
        this.expectedUrl = expected;
    }


    @Parameters
    public static Collection<Object[]> data() {
        final Object[][] params = new Object[][] {
            {
                "https://auth.vt.edu/",
                new MockRequestContext(),
                "https://auth.vt.edu/",
            },
            {
                "/login",
                createRequest("service=https://my.vt.edu/Login", "renew=true"),
                "/login?service=https%3A%2F%2Fmy.vt.edu%2FLogin&renew=true",
            },
            {
                "/?pdc=1",
                createRequest("Service=https%3A%2F%2Fa.vt.edu%3Ffoo%3Dbar", "renew=true"),
                "/?pdc=1&service=https%3A%2F%2Fa.vt.edu%3Ffoo%3Dbar&renew=true",
            },
            {
                "/?pdc=1",
                createRequest("target=https://ä.vt.edu?foo=bar", "SAMLArt=ST-123", "evil=eye"),
                "/?pdc=1&TARGET=https%3A%2F%2F%C3%A4.vt.edu%3Ffoo%3Dbar&SAMLart=ST-123",
            },
            {
                "/",
                createRequest("samlrequest=hVJNb9QwEL0j8R8s3%2BNsIiqQtUm1tKpYqUDUTTlw8zqzjqm%2F8DhZ%2BPd4s61aDpTr85" +
                        "t5H5715S9ryAwRtXcNrdiKEnDSD9qpht73N8UHetm%2BfbNGYU3gmymN7g5%2BToCJ5EmHfHlo6BQd9wI1cicsIE%2B" +
                        "S7zafb3nNVjxEn7z0hpLtdUOFCs4%2FKBgDKGkHdbBWBG%2BC3qsR3DCKHw9g9ntBybcnW%2FXJ1hZxgq3DJFzK0Kqq" +
                        "i1VV1O%2F76h2%2FuOB1%2FZ2S7lHpo3bnBK%2FZ2p9JyD%2F1fVd0X3f9smDWA8Qvmd1Q5b0ywKS3J%2FlOIOo5wwd" +
                        "hECjZIEJM2eCVdzhZiDuIs5Zwf3fb0DGlgLwsj8cje15TijJEyH0MTDErtGFzYjBMpZBI26VkvuSML9p9PYV4ckHb%2F" +
                        "%2BusyxcS7eOnnrJurztvtPxNNsb441UEkXLQFKec88ZHK9K%2FXVSsWhA9FIeFyieHAaQ%2BaBgoKduz6t%2FXk2%2F" +
                        "qDw%3D%3D",
                        "RelayState=https://www.google.com/a/vt.edu/ServiceLogin?service=mail", "foo=bar"),
                "/?SAMLRequest=hVJNb9QwEL0j8R8s3%2BNsIiqQtUm1tKpYqUDUTTlw8zqzjqm%2F8DhZ%2BPd4s61aDpTr85" +
                        "t5H5715S9ryAwRtXcNrdiKEnDSD9qpht73N8UHetm%2BfbNGYU3gmymN7g5%2BToCJ5EmHfHlo6BQd9wI1cicsIE%2B" +
                        "S7zafb3nNVjxEn7z0hpLtdUOFCs4%2FKBgDKGkHdbBWBG%2BC3qsR3DCKHw9g9ntBybcnW%2FXJ1hZxgq3DJFzK0Kqq" +
                        "i1VV1O%2F76h2%2FuOB1%2FZ2S7lHpo3bnBK%2FZ2p9JyD%2F1fVd0X3f9smDWA8Qvmd1Q5b0ywKS3J%2FlOIOo5wwd" +
                        "hECjZIEJM2eCVdzhZiDuIs5Zwf3fb0DGlgLwsj8cje15TijJEyH0MTDErtGFzYjBMpZBI26VkvuSML9p9PYV4ckHb%2F" +
                        "%2BusyxcS7eOnnrJurztvtPxNNsb441UEkXLQFKec88ZHK9K%2FXVSsWhA9FIeFyieHAaQ%2BaBgoKduz6t%2FXk2%2F" +
                        "qDw%3D%3D&RelayState=https%3A%2F%2Fwww.google.com%2Fa%2Fvt.edu%2FServiceLogin%3Fservice%3Dmail",
            },
            {
                "/",
                createRequest("SAMLRequest=hVJNb9QwEL0j8R8s3+NsIiqQtUm1tKpYqUDUTTlw8zqzjqm/8DhZ+Pd4s61aDpTr85t5H5715S" +
                        "9ryAwRtXcNrdiKEnDSD9qpht73N8UHetm+fbNGYU3gmymN7g5+ToCJ5EmHfHlo6BQd9wI1cicsIE+S7zafb3nNVjxEn7" +
                        "z0hpLtdUOFCs4/KBgDKGkHdbBWBG+C3qsR3DCKHw9g9ntBybcnW/XJ1hZxgq3DJFzK0Kqqi1VV1O/76h2/uOB1/Z2S7l" +
                        "Hpo3bnBK/Z2p9JyD/1fVd0X3f9smDWA8Qvmd1Q5b0ywKS3J/lOIOo5wwdhECjZIEJM2eCVdzhZiDuIs5Zwf3fb0DGlgL" +
                        "wsj8cje15TijJEyH0MTDErtGFzYjBMpZBI26VkvuSML9p9PYV4ckHb/+usyxcS7eOnnrJurztvtPxNNsb441UEkXLQFK" +
                        "ec88ZHK9K/XVSsWhA9FIeFyieHAaQ+aBgoKduz6t/Xk2/qDw==",
                        "RelayState=https://www.google.com/a/vt.edu/ServiceLogin?service=mail", "foo=bar"),
                "/?SAMLRequest=hVJNb9QwEL0j8R8s3%2BNsIiqQtUm1tKpYqUDUTTlw8zqzjqm%2F8DhZ%2BPd4s61aDpTr85" +
                        "t5H5715S9ryAwRtXcNrdiKEnDSD9qpht73N8UHetm%2BfbNGYU3gmymN7g5%2BToCJ5EmHfHlo6BQd9wI1cicsIE%2B" +
                        "S7zafb3nNVjxEn7z0hpLtdUOFCs4%2FKBgDKGkHdbBWBG%2BC3qsR3DCKHw9g9ntBybcnW%2FXJ1hZxgq3DJFzK0Kqq" +
                        "i1VV1O%2F76h2%2FuOB1%2FZ2S7lHpo3bnBK%2FZ2p9JyD%2F1fVd0X3f9smDWA8Qvmd1Q5b0ywKS3J%2FlOIOo5wwd" +
                        "hECjZIEJM2eCVdzhZiDuIs5Zwf3fb0DGlgLwsj8cje15TijJEyH0MTDErtGFzYjBMpZBI26VkvuSML9p9PYV4ckHb%2F" +
                        "%2BusyxcS7eOnnrJurztvtPxNNsb441UEkXLQFKec88ZHK9K%2FXVSsWhA9FIeFyieHAaQ%2BaBgoKduz6t%2FXk2%2F" +
                        "qDw%3D%3D&RelayState=https%3A%2F%2Fwww.google.com%2Fa%2Fvt.edu%2FServiceLogin%3Fservice%3Dmail",
            },
        };
        return Arrays.asList(params);
    }


    @Test
    public void testBuild() throws Exception {
        Assert.assertEquals(expectedUrl, UrlBuilder.build(baseUrl, mockRequest));
    }
    
    
    private static RequestContext createRequest(final String ... pairs) {
        final Map<String, String> map = new LinkedHashMap<String, String>(pairs.length);
        for (String pair : pairs) {
            final String[] kv = pair.split("=", 2);
            map.put(kv[0], kv[1]);
        }
        return new MockRequestContext(new LocalParameterMap(map));
    }
}
