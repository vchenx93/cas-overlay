/*
  $Id: $

  Copyright (C) 2012 Virginia Tech.
  All rights reserved.

  SEE LICENSE FOR MORE INFORMATION

  Author:  Middleware Services
  Email:   middleware@vt.edu
  Version: $Revision: $
  Updated: $Date: $
*/
package edu.vt.middleware.cas.web;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

import junit.framework.Assert;
import org.jasig.cas.services.RegexRegisteredService;
import org.jasig.cas.services.RegisteredService;
import org.jasig.cas.services.ReloadableServicesManager;
import org.jasig.cas.services.ServicesManager;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.ui.ExtendedModelMap;
import org.springframework.ui.Model;
import org.springframework.validation.BeanPropertyBindingResult;
import org.springframework.validation.Errors;
import org.springframework.web.servlet.mvc.support.RedirectAttributesModelMap;

/**
 * Unit test for {@link RegisteredServiceController} class.
 *
 * @author  Middleware Services
 * @version $Revision: $
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration({
    "classpath:/applicationContext-test.xml",
    "file:src/main/webapp/WEB-INF/deployerConfigContext.xml"
})
public class RegisteredServiceControllerTest {

    @Autowired
    private RegisteredServiceController controller;

    @Autowired
    private ReloadableServicesManager servicesManager;

    private ExecutorService executor;

    private long concurrentServiceId;

    @Before
    public void setUp() throws Exception {
        this.executor = Executors.newFixedThreadPool(10);
        final RegexRegisteredService service = new RegexRegisteredService();
        service.setServiceId("^https://www.vt.edu/.*");
        service.setName("Test");
        service.setEnabled(true);
        service.setAllowedToProxy(false);
        this.concurrentServiceId = this.servicesManager.save(service).getId();
    }

    @After
    public void tearDown() throws Exception {
        if (this.executor != null && !this.executor.isShutdown()) {
            this.executor.shutdownNow();
        }
    }

    @Test
    public void testConcurrentSave() throws Exception {
        final List<ServiceCreator> runners = new ArrayList<ServiceCreator>(30);
        for (int i = 0; i < 30; i++) {
            runners.add(new ServiceCreator());
        }
        final List<Future<Result>> results = this.executor.invokeAll(runners);
        int failureCount = 0;
        for (Future<Result> result : results) {
            if (result.get().error != null) {
                failureCount++;
            }
        }
        Assert.assertEquals(0, failureCount);
    }

    class ServiceCreator implements Callable<Result> {
        public Result call() throws Exception {
            final RegexRegisteredService service =
                (RegexRegisteredService) servicesManager.findServiceBy(concurrentServiceId);
            service.setName("Modified-" + System.nanoTime());
            final Result result = new Result();
            try {
                controller.save(
                    service,
                    new BeanPropertyBindingResult(service, "service"),
                    new ExtendedModelMap(),
                    new RedirectAttributesModelMap());
                result.service = service;
            } catch (Exception e) {
                result.error = e;
            }
            return result;
        }
    }

    class Result {
        public RegisteredService service;
        public Exception error;
    }
}
