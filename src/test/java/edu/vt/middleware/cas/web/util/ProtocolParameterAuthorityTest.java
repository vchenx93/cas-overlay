/*
  $Id: $

  Copyright (C) 2012 Virginia Tech.
  All rights reserved.

  SEE LICENSE FOR MORE INFORMATION

  Author:  Middleware Services
  Email:   middleware@vt.edu
  Version: $Revision: $
  Updated: $Date: $
*/
package edu.vt.middleware.cas.web.util;

import java.util.LinkedHashMap;
import java.util.Map;


import org.junit.Test;
import org.springframework.webflow.core.collection.LocalParameterMap;
import org.springframework.webflow.test.MockFlowExecutionContext;
import org.springframework.webflow.test.MockRequestContext;

import static org.junit.Assert.*;

/**
 * Unit test for {@link ProtocolParameterAuthority} class.
 *
 * @author Middleware Services
 * @version: $Revision: $
 */
public class ProtocolParameterAuthorityTest {
    @Test
    public void testGetParameters() throws Exception {
        final MockRequestContext context = new MockRequestContext();
        context.putRequestParameter("SAMLArt", "A");
        context.putRequestParameter("gateway", "false");
        context.putRequestParameter("TARGET", "b");
        context.putRequestParameter("q", "fruitcake");
        context.putRequestParameter("Renew", "true");
        final Map<String, String> expected = new LinkedHashMap<String, String>();
        expected.put("SAMLart", "A");
        expected.put("gateway", "false");
        expected.put("TARGET", "b");
        expected.put("renew", "true");
        assertEquals(expected, ProtocolParameterAuthority.getParameters(context));
    }

    @Test
    public void testSaveParameters() throws Exception {
        final Map<String, String> map = new LinkedHashMap<String, String>();
        map.put("SAMLRequest", "A");
        map.put("flavor", "pistacchio");
        map.put("RelayState", "B");
        map.put("renew", "true");
        final MockFlowExecutionContext flowExecutionContext = new MockFlowExecutionContext();
        final MockRequestContext context1 = new MockRequestContext(new LocalParameterMap(map));
        context1.setFlowExecutionContext(flowExecutionContext);
        ProtocolParameterAuthority.saveParameters(context1);

        // Create a new request context that relies on flow execution context for param retrieval
        final MockRequestContext context2 = new MockRequestContext(new LocalParameterMap(map));
        context2.setFlowExecutionContext(flowExecutionContext);

        assertEquals("A", ProtocolParameterAuthority.getParameter("samlrequest", context2));
        assertEquals("B", ProtocolParameterAuthority.getParameter("relaySTATE", context2));
        assertEquals("true", ProtocolParameterAuthority.getParameter("reneW", context2));
    }
}
