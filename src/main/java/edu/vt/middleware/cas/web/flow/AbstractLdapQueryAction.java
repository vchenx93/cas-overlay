/*
  $Id$

  Copyright (C) 2009 Virginia Tech.
  All rights reserved.

  SEE LICENSE FOR MORE INFORMATION

  Author:  Middleware
  Email:   middleware@vt.edu
  Version: $Revision$
  Updated: $Date$
*/
package edu.vt.middleware.cas.web.flow;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import javax.naming.NamingException;
import javax.naming.directory.Attribute;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import org.springframework.beans.factory.annotation.Required;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.ldap.core.ContextMapper;
import org.springframework.ldap.core.DirContextAdapter;
import org.springframework.ldap.core.LdapEncoder;
import org.springframework.ldap.core.LdapTemplate;
import org.springframework.webflow.execution.RequestContext;

/**
 * Base class for actions that perform a simple LDAP query to determine a yes/no
 * value based the value of a single attribute.
 *
 * @author Marvin S. Addison
 * @version $Revision$
 *
 */
public abstract class AbstractLdapQueryAction {
    /** Yes action result. */
    public static final String YES = "yes";

    /** No action result. */
    public static final String NO = "no";

    /** Describes a missing attribute in the LDAP query result set. */
    protected static final TaggedResult ATTR_NOT_FOUND =
        new TaggedResult("ATTRIBUTE_NOT_FOUND");

    /** LDAP search base. */
    private static final String SEARCH_BASE = "ou=People,dc=vt,dc=edu";

    /** Logger instance. */
    protected final Log logger = LogFactory.getLog(getClass());

    /** LDAP template use to perform LDAP query. */
    private LdapTemplate ldapTemplate;


    /** Maps the value of a single LDAP attribute onto a String. */
    private final ContextMapper resultMapper = new ContextMapper() {
        private final Set<String> attributes = getAttributes();

        public Object mapFromContext(final Object o) {
            final DirContextAdapter adapter = (DirContextAdapter) o;
            final Map<String, Object> values = new HashMap<String, Object>();
            try {
                for (String name : attributes) {
                    final Attribute attr = adapter.getAttributes().get(name);
                    values.put(name,
                            attr != null ? attr.get() : ATTR_NOT_FOUND);
                }
                return values;
            } catch (NamingException e) {
                throw new RuntimeException(
                        "Attribute error for " + adapter.getDn(), e);
            }
        }
    };


    /**
     * @param template  LDAP template to perform passwordState query.
     */
    @Required
    public void setLdapTemplate(final LdapTemplate template) {
        this.ldapTemplate = template;
    }


    /**
     * Executes an LDAP query for user attributes using the given username
     * as the basis of the query and produces a yes/no result.
     *
     * @param context Spring Webflow request context.
     * @param username Username to search for.
     *
     * @return  "yes" when attribute meets implementation-specific criteria,
     * "no" otherwise.
     */
    @SuppressWarnings("unchecked")
    public String searchForUser(
            final RequestContext context,
            final String username) {

        if (!preHandleContext(context, username)) {
            return NO;
        }

        final String query = getQuery(LdapEncoder.filterEncode(username));
        if (logger.isDebugEnabled()) {
            logger.debug(String.format(
                "Executing query %s for attributes %s.",
                query,
                getAttributes()));
        }
        Map<String, Object> result;
        try {
            result = (Map<String, Object>) ldapTemplate.searchForObject(
                    SEARCH_BASE, query, resultMapper);
        } catch (EmptyResultDataAccessException empty) {
            result = Collections.emptyMap();
        }

        if (logger.isDebugEnabled()) {
            logger.debug(String.format(
                "Query %s produced result %s.", query, result));
        }
        postHandleContext(context, username, result);

        final String flowResult = convertResult(context, result) ? YES : NO;
        logResult(username, flowResult);
        return flowResult;
    }


    /**
     * Perform Webflow request context setup prior to LDAP query execution.
     *
     * @param context Spring Webflow request context.
     * @param username  Username to query for.
     *
     * @return  True to signal continue, false to signal that query execution
     * should abort.
     */
    protected boolean preHandleContext(
            final RequestContext context, final String username) {
        return true;
    }


    /**
     * Perform Webflow request context setup after LDAP query execution.
     *
     * @param context Spring Webflow request context.
     * @param username  Username to query for.
     * @param resultMap  Map of attribute name:value pairs from LDAP query.
     */
    protected void postHandleContext(
        final RequestContext context,
        final String username,
        final Map<String, Object> resultMap) {}


    /**
     * Gets the set of attributes to query for.
     *
     * @return  Attributes to query for.
     */
    protected abstract Set<String> getAttributes();


    /**
     * Gets LDAP query to execute for the given user.
     *
     * @param  username  Username of authentication credential.
     *
     * @return  LDAP query string.
     */
    protected abstract String getQuery(String username);


    /**
     * Converts the LDAP query results to a boolean.
     *
     * @param  context  Spring Webflow request context.
     * @param  resultMap  Result of LDAP query to map onto boolean.
     *
     * @return  Boolean equivalent of query result.
     */
    protected abstract boolean convertResult(RequestContext context, Map<String, Object> resultMap);


    /**
     * Logs a message about the result of executing the flow action.
     *
     * @param authId User ID of user action is executed on.
     * @param result String result of flow action execution.
     */
    protected abstract void logResult(String authId, String result);


    /**
     * Type supports special result conditions for LDAP queries.
     */
    protected static final class TaggedResult {
        /** Tag name */
        private final String tag;

        /**
         * Creates a new instance with the given tag.
         * @param name Tag name.
         */
        public TaggedResult(final String name) {
            tag = name;
        }

        /** {@inheritDoc} */
        @Override
        public String toString() {
            return tag;
        }
    }
}
