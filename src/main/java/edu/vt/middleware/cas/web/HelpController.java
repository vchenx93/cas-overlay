/*
  $Id$

  Copyright (C) 2009 Virginia Tech.
  All rights reserved.

  SEE LICENSE FOR MORE INFORMATION

  Author:  Middleware
  Email:   middleware@vt.edu
  Version: $Revision$
  Updated: $Date$
*/
package edu.vt.middleware.cas.web;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Required;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.ParameterizableViewController;

/**
 * Help controller.
 *
 * @author Marvin S. Addison
 * @version $Revision$
 *
 */
public class HelpController extends ParameterizableViewController {

    /** URL to account recovery app -- AKA password reset. */
    private String accountRecoveryUrl;

    /** URL to account recovery FAQ. */
    private String accountRecoveryFaqUrl;

    /** URL to account recovery management app. */
    private String accountRecoveryManagementUrl;


    /**
     * @param url URL to the account recovery management application.
     */
    @Required
    public void setAccountRecoveryManagementUrl(final String url) {
        this.accountRecoveryManagementUrl = url;
    }


    /**
     * @param url URL to the account recovery application.
     */
    @Required
    public void setAccountRecoveryUrl(final String url) {
        this.accountRecoveryUrl = url;
    }


    /**
     * @param url URL to account recovery FAQ.
     */
    public void setAccountRecoveryFaqUrl(final String url) {
        this.accountRecoveryFaqUrl = url;
    }


    /** {@inheritDoc} */
    @Override
    protected ModelAndView handleRequestInternal(
        final HttpServletRequest request,
        final HttpServletResponse response) throws Exception {

        final Map<String, Object> model = new HashMap<String, Object>();
        model.put("accountRecoveryUrl", accountRecoveryUrl);
        model.put("accountRecoveryFaqUrl", accountRecoveryFaqUrl);
        model.put("accountRecoveryManagementUrl", accountRecoveryManagementUrl);
        return new ModelAndView(getViewName(), "model", model);
    }

}
