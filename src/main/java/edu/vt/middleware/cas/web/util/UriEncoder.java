/*
  $Id: $

  Copyright (C) 2012 Virginia Tech.
  All rights reserved.

  SEE LICENSE FOR MORE INFORMATION

  Author:  Middleware Services
  Email:   middleware@vt.edu
  Version: $Revision: $
  Updated: $Date: $
*/
package edu.vt.middleware.cas.web.util;

import java.nio.charset.Charset;

/**
 * Utility class to encode a string according to rules RFC-3986.
 *
 * @author Middleware Services
 * @version $Revision: $
 */
public final class UriEncoder {
    /** Hex digits. */
    private static final char[] HEX_DIGITS = {
        '0', '1', '2', '3', '4', '5', '6', '7',
        '8', '9', 'A', 'B', 'C', 'D', 'E', 'F',
    };

    /** URI reserved characters. */
    private static final String RESERVED = " :/?#[]@!$&'()*+,;=";

    /** UTF-8 character set. */
    private static final Charset UTF_8 = Charset.forName("UTF-8");


    /** Protect constructor of utility class. */
    private UriEncoder() {}


    /**
     * Encode the given string using URI escaping rules.
     *
     * @param s String to encode.
     *
     * @return Encoded string.
     */
    public static String encode(final String s) {
        final StringBuilder sb = new StringBuilder();
        final byte[] bytes = s.getBytes(UTF_8);
        int b;
        // CheckStyle:MagicNumber OFF
        for (int i = 0; i < bytes.length; i++) {
            b = bytes[i] & 0xFF;
            if (RESERVED.indexOf(b) > -1 || b >= 0x80) {
                escape(sb, b);
            } else {
                sb.append((char) b);
            }
        }
        // CheckStyle:MagicNumber ON
        return sb.toString();
    }


    /**
     * Escapes the given byte and appends it to the given StringBuilder.
     *
     * @param sb String builder.
     * @param c Character (as a byte) to escape.
     */
    private static void escape(final StringBuilder sb, final int c) {
        sb.append('%');
        sb.append(HEX_DIGITS[(c >> 4) & 0x0F]);
        sb.append(HEX_DIGITS[c & 0x0F]);
    }
}
