/*
  $Id$

  Copyright (C) 2009 Virginia Tech.
  All rights reserved.

  SEE LICENSE FOR MORE INFORMATION

  Author:  Middleware
  Email:   middleware@vt.edu
  Version: $Revision$
  Updated: $Date$
*/
package edu.vt.middleware.cas.web.flow;

import javax.servlet.http.HttpServletResponse;

import javax.validation.constraints.NotNull;

import org.jasig.cas.web.support.WebUtils;

import org.springframework.util.StringUtils;
import org.springframework.web.util.CookieGenerator;
import org.springframework.webflow.action.AbstractAction;
import org.springframework.webflow.execution.Event;
import org.springframework.webflow.execution.RequestContext;

/**
 * Sets the warn cookie if the "warn" parameter is present in the request
 * context.
 *
 * @author Middleware
 * @version $Revision$
 *
 */
public class WarnCookieHandlerAction extends AbstractAction {

    /** Generates warn cookie values */
    @NotNull
    private CookieGenerator warnCookieGenerator;


    /**
     * @param  generator  Warn cookie generator;
     */
    public final void setWarnCookieGenerator(final CookieGenerator generator) {
        this.warnCookieGenerator = generator;
    }


    /** {@inheritDoc} */
    @Override
    protected Event doExecute(final RequestContext context) throws Exception {
        final HttpServletResponse response =
            WebUtils.getHttpServletResponse(context);

        if (StringUtils.hasText(context.getExternalContext()
                .getRequestParameterMap().get("warn"))) {
            logger.info("Setting warn cookie.");
            this.warnCookieGenerator.addCookie(response, "true");
        } else {
            this.warnCookieGenerator.removeCookie(response);
        }
        return success();
    }

}
