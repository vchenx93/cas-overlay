/*
  $Id$

  Copyright (C) 2009 Virginia Tech.
  All rights reserved.

  SEE LICENSE FOR MORE INFORMATION

  Author:  Middleware
  Email:   middleware@vt.edu
  Version: $Revision$
  Updated: $Date$
*/
package edu.vt.middleware.cas.web;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Required;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.ParameterizableViewController;

/**
 * Provides an alternate implementation of displaying the warn page on
 * a separate URL than the CAS login webflow.
 *
 * @author Middleware
 * @version $Revision$
 *
 */
public class WarnController extends ParameterizableViewController {

    /** Login URL */
    private String loginUrl;


    /**
     * @param  url  CAS login URL.
     */
    @Required
    public void setLoginUrl(final String url) {
        this.loginUrl = url;
    }


    /** {@inheritDoc} */
    @Override
    protected ModelAndView handleRequestInternal(
        final HttpServletRequest request,
        final HttpServletResponse response)
        throws Exception {

        final String serviceTicketId = request.getParameter("ticket");
        if (serviceTicketId == null) {
            return new ModelAndView("redirect:" + loginUrl);
        }
        return new ModelAndView(
                getViewName(), "serviceTicketId", serviceTicketId);
    }

}
