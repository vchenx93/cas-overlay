/*
  $Id$

  Copyright (C) 2009 Virginia Tech.
  All rights reserved.

  SEE LICENSE FOR MORE INFORMATION

  Author:  Middleware
  Email:   middleware@vt.edu
  Version: $Revision$
  Updated: $Date$
*/
package edu.vt.middleware.cas.web.flow;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import edu.vt.middleware.cas.web.util.ProtocolParameterAuthority;
import edu.vt.middleware.cas.web.util.UriEncoder;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.webflow.execution.RequestContext;

/**
 * Checks the ED passwordState attribute on failed authentication to determine
 * whether password expiration is the cause of failure.  Returns "yes" if the
 * password is expired, "no" otherwise.
 *
 * @author Marvin S. Addison
 * @version $Revision$
 *
 */
public class CheckPasswordExpirationAction extends AbstractLdapQueryAction {

    /** Password state attribute. */
    private static final String PASSWORD_STATE = "passwordState";

    /** Person type attribute. */
    private static final String PERSON_TYPE = "personType";

    /** Set of attributes to query. */
    private static final Set<String> ATTRIBUTES = new HashSet<String>(Arrays.asList(PASSWORD_STATE, PERSON_TYPE));

    /** Expired password state value. */
    private static final String EXPIRED = "EXPIRED";

    /** URL of password change application for VT constituents. */
    private String passwordChangeUrl;

    /** URL of password change application for guests. */
    private String guestPasswordChangeUrl;


    /**
     * @param url URL to password change application for VT constituents.
     */
    @Required
    public void setPasswordChangeUrl(final String url) {
        this.passwordChangeUrl = url;
    }


    /**
     * @param url URL to password change application for guests.
     */
    @Required
    public void setGuestPasswordChangeUrl(final String url) {
        this.guestPasswordChangeUrl = url;
    }


    /** {@inheritDoc} */
    @Override
    protected boolean preHandleContext(final RequestContext context, final String username) {
        context.getExternalContext().getSessionMap().put("username", username);
        return true;
    }


    /** {@inheritDoc} */
    @Override
    protected void postHandleContext(
        final RequestContext context,
        final String username,
        final Map<String, Object> resultMap) {

        if (resultMap.isEmpty()) {
            return;
        }
        final String personType = (String) resultMap.get(PERSON_TYPE);
        final StringBuilder url = new StringBuilder();
        if (personType.contains("Guest")) {
            url.append(guestPasswordChangeUrl).append("?guestId=")
                .append(username);
        } else {
            url.append(passwordChangeUrl).append("?pid=").append(username);
        }
        final String service = ProtocolParameterAuthority.getServiceId(context);
        if (service != null) {
            url.append("&return_to=").append(UriEncoder.encode(service));
        }
        context.getRequestScope().put("passwordChangeUrl", url.toString());
    }


    /** {@inheritDoc} */
    protected Set<String> getAttributes() {
        return ATTRIBUTES;
    }


    /** {@inheritDoc} */
    protected String getQuery(final String username) {
        return "authId=" + username;
    }


    /** {@inheritDoc} */
    protected boolean convertResult(final RequestContext context, final Map<String, Object> resultMap) {
        return EXPIRED.equals(resultMap.get(PASSWORD_STATE));
    }


    /** {@inheritDoc} */
    protected void logResult(final String authId, final String result) {
        if (YES.equals(result)) {
            logger.info("Password expired for " + authId);
        }
    }
}
