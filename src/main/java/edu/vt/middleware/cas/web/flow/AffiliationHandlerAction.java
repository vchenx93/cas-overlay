/*
  $Id$

  Copyright (C) 2011 Virginia Tech.
  All rights reserved.

  SEE LICENSE FOR MORE INFORMATION

  Author:  Middleware
  Email:   middleware@vt.edu
  Version: $Revision$
  Updated: $Date$
*/
package edu.vt.middleware.cas.web.flow;

import org.springframework.webflow.execution.RequestContext;

/**
 * Webflow action for processing the affiliation type selected on the username
 * help form.
 *
 * @author Marvin S. Addison
 * @version $Revision$
 *
 */
public class AffiliationHandlerAction
    extends AbstractForgottenCredentialAction {

    /** Guest affiliation. */
    public static final String GUEST_AFFILIATION = "guest";

    /** Virginia Tech affiliation. */
    public static final String VT_AFFILIATION = "vt";


    /**
     * Process the user's affiliation.
     *
     * @param  context  SWF request context.
     *
     * @return  String result indicating next action.
     */
    public String processAffiliation(final RequestContext context) {
        final String type = context.getRequestParameters().get("affiliation");

        if (VT_AFFILIATION.equals(type)) {
            return VT_AFFILIATION;
        } else if (GUEST_AFFILIATION.equals(type)) {
            return GUEST_AFFILIATION;
        }
        context.getMessageContext().addMessage(
            newErrorMsg(null, "iforgot.error.affiliation"));
        return UNKNOWN;
    }
}
