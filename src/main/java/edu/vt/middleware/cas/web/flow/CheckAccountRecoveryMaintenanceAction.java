/*
  $Id$

  Copyright (C) 2009 Virginia Tech.
  All rights reserved.

  SEE LICENSE FOR MORE INFORMATION

  Author:  Middleware
  Email:   middleware@vt.edu
  Version: $Revision$
  Updated: $Date$
*/
package edu.vt.middleware.cas.web.flow;

import java.util.Collections;
import java.util.Map;
import java.util.Set;

import edu.vt.middleware.cas.web.util.ProtocolParameterAuthority;
import edu.vt.middleware.cas.web.util.UriEncoder;
import org.jasig.cas.CentralAuthenticationService;
import org.jasig.cas.web.support.CookieRetrievingCookieGenerator;
import org.jasig.cas.web.support.WebUtils;
import org.joda.time.DateTime;
import org.joda.time.Duration;
import org.joda.time.format.DateTimeFormatter;
import org.joda.time.format.ISODateTimeFormat;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.webflow.execution.RequestContext;

/**
 * Checks the ED accountRecoveryMaintenanceDate attribute on successful
 * authentication to determine whether maintenance is needed on account
 * recovery preferences.  Returns "yes" if account maintenance is needed due
 * to the preference maintenance period elapsing, "no" otherwise.
 *
 * @author Marvin S. Addison
 * @version $Revision$
 *
 */
public class CheckAccountRecoveryMaintenanceAction
    extends AbstractLdapQueryAction {

    /** Account recovery maintenance date attribute name. */
    private static final String MAINT_DATE = "accountRecoveryMaintenanceDate";

    /** Set of attributes to query. */
    private static final Set<String> ATTRIBUTES = Collections.singleton(MAINT_DATE);

    /**
     * LDAP query template.
     * NOTE: The inclusion of virginiaTechId causes both guests and sponsored
     * accounts to be excluded from password reset workflow.
     */
    private static final String QUERY_TEMPLATE = "(&(authId=%s)(virginiaTechId=*))";

    /** Date formatter. */
    private static final DateTimeFormatter FORMATTER = ISODateTimeFormat.dateHourMinuteSecond();

    /** TGT cookie generator */
    private CookieRetrievingCookieGenerator ticketGrantingTicketCookieGenerator;

    /** Global instance of CAS. */
    private CentralAuthenticationService centralAuthenticationService;

    /** URL to account management app set initial preferences area. */
    private String accountRecoveryManagementSetUrl;

    /** URL to account management app verify preferences area. */
    private String accountRecoveryManagementVerifyUrl;

    /** Threshold period of account recovery maintenance. */
    private Duration maxInactivityPeriod;


    /**
     * Sets the TGT cookie generator.
     *
     * @param generator Cookie generator.
     */
    @Required
    public void setTicketGrantingTicketCookieGenerator(final CookieRetrievingCookieGenerator generator) {
        this.ticketGrantingTicketCookieGenerator = generator;
    }


    /**
     * Sets the CAS instance to support dealing with TGT in cookies.
     * @param cas Global CAS instance.
     */
    @Required
    public void setCentralAuthenticationService(final CentralAuthenticationService cas) {
        this.centralAuthenticationService = cas;
    }


    /**
     * @param url URL to the area of the account recovery management application
     * where initial preferences are set.
     */
    @Required
    public void setAccountRecoveryManagementSetUrl(final String url) {
        this.accountRecoveryManagementSetUrl = url;
    }


    /**
     * @param url URL to the area of the account recovery management application
     * where existing preferences are verified with option to change.
     */
    @Required
    public void setAccountRecoveryManagementVerifyUrl(final String url) {
        this.accountRecoveryManagementVerifyUrl = url;
    }


    /**
     * Sets the maximum number of days of inactivity on maintenance of account
     * recovery preferences after which the user will be nagged to update
     * preferences.
     *
     * @param  days  Maximum inactivity period in days; MUST be positive integer.
     */
    @Required
    public void setMaxInactivityPeriod(final int days) {
        if (days < 1) {
            throw new IllegalArgumentException(
                    "Inactivity period must be positive integer.");
        }
        this.maxInactivityPeriod = Duration.standardDays(days);
    }


    /** {@inheritDoc} */
    @Override
    protected boolean preHandleContext(final RequestContext context, final String username) {
        // Must set the TGT cookie to signal successful authentication
        final String tgtId = WebUtils.getTicketGrantingTicketId(context);
        final String tgtIdFromCookie = (String) context.getFlowScope().get("ticketGrantingTicketId");
        if (tgtId != null) {
            this.ticketGrantingTicketCookieGenerator.addCookie(
                    WebUtils.getHttpServletRequest(context),
                    WebUtils.getHttpServletResponse(context),
                    tgtId);
        }

        if (tgtIdFromCookie != null && !tgtId.equals(tgtIdFromCookie)) {
            // If a previous cookie was provided and a new one was issued,
            // the old should be destroyed
            this.centralAuthenticationService.destroyTicketGrantingTicket(tgtIdFromCookie);
        }
        context.getRequestScope().put("maxDays", maxInactivityPeriod.toStandardDays().getDays());

        return true;
    }


    /** {@inheritDoc} */
    @Override
    protected void postHandleContext(
        final RequestContext context,
        final String username,
        final Map<String, Object> resultMap) {

        final Object result = resultMap.get(MAINT_DATE);
        final StringBuilder url = new StringBuilder();
        if (ATTR_NOT_FOUND.equals(result)) {
            url.append(this.accountRecoveryManagementSetUrl);
        } else {
            url.append(this.accountRecoveryManagementVerifyUrl);
            context.getRequestScope().put(MAINT_DATE, result);
        }
        final String service = ProtocolParameterAuthority.getServiceId(context);
        if (service != null) {
            url.append("?return_to=").append(UriEncoder.encode(service));
        }
        context.getRequestScope().put("accountRecoveryManagementUrl", url.toString());

    }


    /** {@inheritDoc} */
    protected Set<String> getAttributes() {
        return ATTRIBUTES;
    }


    /** {@inheritDoc} */
    protected String getQuery(final String username) {
        return String.format(QUERY_TEMPLATE, username);
    }


    /** {@inheritDoc} */
    protected boolean convertResult(final RequestContext context, final Map<String, Object> resultMap) {
        final Object result = resultMap.get(MAINT_DATE);
        if (ATTR_NOT_FOUND.equals(result)) {
            // Null date indicates preferences have never been set
            // and maintenance is required.
            return true;
        } else if (resultMap.isEmpty()) {
            // An empty result set indicates the user could not be found
            // and account maintenance does not apply
            return false;
        }
        final DateTime lastChangeDate = FORMATTER.parseDateTime((String) result);
        return lastChangeDate.plus(maxInactivityPeriod).isBefore(DateTime.now());
    }


    /** {@inheritDoc} */
    protected void logResult(final String authId, final String result) {
        if (YES.equals(result)) {
            logger.info("Account recovery maintenance required for " + authId);
        }
    }

}
