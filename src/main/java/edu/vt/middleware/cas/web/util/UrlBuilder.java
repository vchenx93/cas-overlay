/*
  $Id: $

  Copyright (C) 2012 Virginia Tech.
  All rights reserved.

  SEE LICENSE FOR MORE INFORMATION

  Author:  Middleware Services
  Email:   middleware@vt.edu
  Version: $Revision: $
  Updated: $Date: $
*/
package edu.vt.middleware.cas.web.util;

import java.util.Map;

import org.springframework.webflow.execution.RequestContext;

/**
 * Utility class that builds URLs to CAS resources that respect CAS protocol
 * parameters while omitting other parameters.
 *
 * @author Middleware Services
 * @version $Revision: $
 */
public final class UrlBuilder {
    /** Protect constructor of utility class. */
    private UrlBuilder() {}


    /**
     * Builds a URL to a CAS resource given a base URL and request.
     *
     * @param baseUrl Base URL to CAS resource.
     * @param context HTTP request context.
     *
     * @return Base URL with any supported parameters contained in request
     * parameters or session data appended.
     */
    public static String build(final String baseUrl, final RequestContext context) {

        final StringBuilder sb = new StringBuilder(baseUrl);
        String value;
        char separator = baseUrl.indexOf('?') > -1 ? '&' : '?';
        final Map<String, String> protocolParams = ProtocolParameterAuthority.getParameters(context);
        for (String key : protocolParams.keySet()) {
            if (ProtocolParameterAuthority.needsUriEncoding(key)) {
                value = UriEncoder.encode(protocolParams.get(key));
            } else {
                value = protocolParams.get(key);
            }
            sb.append(separator).append(key).append('=').append(value);
            separator = '&';
        }
        return sb.toString();
    }

}
