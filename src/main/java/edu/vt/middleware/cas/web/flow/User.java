/*
  $Id$

  Copyright (C) 2011 Virginia Tech.
  All rights reserved.

  SEE LICENSE FOR MORE INFORMATION

  Author:  Middleware
  Email:   middleware@vt.edu
  Version: $Revision$
  Updated: $Date$
*/
package edu.vt.middleware.cas.web.flow;

/**
 * Simple class that describes a user.
 *
 * @author Marvin S. Addison
 * @version $Revision$
 *
 */
public class User {
    /** Person type. */
    private String personType;

    /** User name. */
    private String username;

    /** User's display name. */
    private String displayName;


    /**
     * Creates a new user whose display name is their username.
     *
     * @param  type  Person type.
     * @param  name  Username.
     */
    public User(final String name, final String type) {
        this.username = name;
        this.personType = type;
        this.displayName = name;
    }


    /**
     * Creates a new user with a distinct display name.
     *
     * @param  name  Username.
     * @param  type  Person type.
     * @param  display  Display name.  If an empty string or whitespace-only
     * string is provided, it is treated as a null value.
     */
    public User(final String name, final String type, final String display) {
        this.username = name;
        this.personType = type;
        if (display != null && display.trim().length() > 0) {
            this.displayName = display;
        }
    }


    /**
     * @return the personType
     */
    public String getPersonType() {
        return personType;
    }


    /**
     * @return the username
     */
    public String getUsername() {
        return username;
    }


    /**
     * @return the displayName
     */
    public String getDisplayName() {
        return displayName;
    }


    /**
     * @param display the displayName to set
     */
    public void setDisplayName(final String display) {
        this.displayName = display;
    }


    /**
     * Determines whether this user is a guest.
     *
     * @return  True if user is guest, false otherwise (including when the
     * person type is indeterminate.)
     */
    public boolean isGuest() {
        return personType != null && personType.contains("Guest");
    }
}
