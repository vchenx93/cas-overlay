/*
  $Id: $

  Copyright (C) 2012 Virginia Tech.
  All rights reserved.

  SEE LICENSE FOR MORE INFORMATION

  Author:  Middleware Services
  Email:   middleware@vt.edu
  Version: $Revision: $
  Updated: $Date: $
*/
package edu.vt.middleware.cas.web.util;

import java.lang.reflect.Field;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.Map;
import java.util.Set;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.jasig.cas.authentication.principal.GoogleAccountsService;
import org.jasig.cas.authentication.principal.Service;
import org.jasig.cas.web.support.WebUtils;
import org.springframework.webflow.execution.RequestContext;

/**
 * The authority on CAS protocol parameters.  Provides services for recording
 * and retrieving CAS protocol parameters from requests.  Protocol parameters
 * may be recorded in session storage in support of durable storage for
 * extended flows such as forgotten credential workflow.
 *
 * @author Middleware Services
 * @version $Revision: $
 */
public final class ProtocolParameterAuthority {
    /** Logger instance. */
    private static final Log LOGGER = LogFactory.getLog(ProtocolParameterAuthority.class);

    /** Set of all supported CAS protocol parameters. */
    private static final Set<String> PARAMS = new LinkedHashSet<String>();

    /** Prefix of session attributes managed by this class. */
    private static final String PROTOCOL_PREFIX = "CAS_PROTOCOL.";

    /** GoogleAccountService#relayState */
    private static final Field GOOGLE_RELAY_STATE_FIELD;


    /** Protect constructor of utility class. */
    private ProtocolParameterAuthority() {}


    /** Class initializer. */
    static {
        PARAMS.add("gateway");
        PARAMS.add("RelayState");
        PARAMS.add("renew");
        PARAMS.add("SAMLart");
        PARAMS.add("SAMLRequest");
        PARAMS.add("service");
        PARAMS.add("TARGET");

        try {
            // Get RelayState reflectively since it's not publicly available
            GOOGLE_RELAY_STATE_FIELD = GoogleAccountsService.class.getDeclaredField("relayState");
            GOOGLE_RELAY_STATE_FIELD.setAccessible(true);
        } catch (NoSuchFieldException e) {
            throw new RuntimeException("GoogleAccountService#relayState does not exist.");
        }
    }


    /**
     * Determines whether the request contains the given protocol parameter
     * by checking both request parameters (POST, query string) and session
     * storage.
     *
     * @param name Case-insensitive parameter name.
     * @param context HTTP request context.
     *
     * @return True if parameter has a value in request, false otherwise.
     */
    public static boolean hasParameter(final String name, final RequestContext context) {
        return getParameter(name, context) != null;
    }


    /**
     * Determines whether the given parameter needs to be URI encoded.
     *
     * @param name Parameter name.
     *
     * @return True if case-insensitive parameter name contains data that may require URI encoding, false otherwise.
     */
    public static boolean needsUriEncoding(final String name) {
        return "service".equalsIgnoreCase(name)
                || "TARGET".equalsIgnoreCase(name)
                || "RelayState".equalsIgnoreCase(name)
                || "SAMLRequest".equalsIgnoreCase(name);
    }


    /**
     * Gets the service identifier from the Webflow context.
     *
     * @param context Spring Webflow execution context.
     *
     * @return Service identifier (URL) or null if no service is defined.
     */
    public static String getServiceId(final RequestContext context) {
        final Service service = WebUtils.getService(context);
        String id = null;
        if (service instanceof GoogleAccountsService) {
            try {
                id = (String) GOOGLE_RELAY_STATE_FIELD.get(service);
            } catch (IllegalAccessException e) {
                LOGGER.warn("Failed getting RelayState from Google service.");
            }
        } else if (service != null) {
            id = service.getId();
        }
        return id;
    }


    /**
     * Gets the given protocol parameter from the request.
     * Both request parameters (POST, query string) and session data bound to
     * the given request are examined for parameters.
     *
     * @param name Case-insensitive parameter name.
     * @param context HTTP request context.
     *
     * @return Parameter value if it exists in request, otherwise null.
     */
    public static String getParameter(final String name, final RequestContext context) {

        return getParameters(context).get(normalizeParameterName(name));
    }


    /**
     * Gets a map of all protocol parameters contained in the given request.
     * Both request parameters and flow scope are examined.
     *
     * @param context HTTP request context.
     *
     * @return Map of protocol parameters in request.  Map items are sorted
     * alphabetically by parameter name and parameter names are normalized to
     * the appropriate case.
     */
    public static Map<String, String> getParameters(final RequestContext context) {
        final Map<String, String> paramMap = new LinkedHashMap<String, String>();
        String value;

        // Check request parameters for protocol param values
        final Map<String, String> paramNameMap = normalizeParameterNames(context);
        for (String normalized : paramNameMap.keySet()) {
            value = context.getRequestParameters().get(paramNameMap.get(normalized));
            if (value != null) {
                paramMap.put(normalized, value);
            }
        }

        // Check flow scope for stored parameters
        String name;
        for (Object key : context.getFlowScope().asMap().keySet()) {
            if (key instanceof String) {
                name = (String) key;
                if (name.startsWith(PROTOCOL_PREFIX)) {
                    value = (String) context.getFlowScope().get(name);
                    if (value != null) {
                        paramMap.put(name.substring(PROTOCOL_PREFIX.length()), value);
                    }
                }
            }
        }

        return paramMap;
    }


    /**
     * Saves all CAS protocol parameters contained in request parameters of
     * the given request into durable session storage.
     *
     * @param context HTTP request context.
     */
    public static void saveParameters(final RequestContext context) {
        final Map<String, String> nameMap = normalizeParameterNames(context);
        for (String normalized : nameMap.keySet()) {
            context.getFlowScope().put(
                PROTOCOL_PREFIX + normalized, context.getRequestParameters().get(nameMap.get(normalized)));
        }
    }


    /**
     * Normalizes protocol parameter names with respect to case.
     *
     * @param name Parameter name.
     *
     * @return Normalized protocol parameter name or null if not a CAS protocol parameter.
     * name.
     */
    private static String normalizeParameterName(final String name) {
        for (String protocolParameter : PARAMS) {
            if (protocolParameter.equalsIgnoreCase(name)) {
                return protocolParameter;
            }
        }
        return null;
    }


    /**
     * Normalizes all protocol parameters in the given request with respect to case.
     *
     * @param context HTTP request context.
     *
     * @return Map of request parameters keyed on normalized parameter. Parameter order is preserved in returned map.
     */
    private static Map<String, String> normalizeParameterNames(final RequestContext context) {
        final Map<String, String> paramMap = new LinkedHashMap<String, String>();
        String name;
        String normalized;
        for (Object key : context.getRequestParameters().asMap().keySet()) {
            name = (String) key;
            normalized = normalizeParameterName(name);
            if (normalized != null) {
                paramMap.put(normalized, name);
            }

        }
        return paramMap;
    }
}
