/*
  $Id$

  Copyright (C) 2009 Virginia Tech.
  All rights reserved.

  SEE LICENSE FOR MORE INFORMATION

  Author:  Middleware
  Email:   middleware@vt.edu
  Version: $Revision$
  Updated: $Date$
*/
package edu.vt.middleware.cas.web.flow;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import edu.vt.middleware.cas.web.util.ProtocolParameterAuthority;
import edu.vt.middleware.cas.web.util.UriEncoder;
import org.jasig.cas.CentralAuthenticationService;
import org.jasig.cas.web.support.CookieRetrievingCookieGenerator;
import org.jasig.cas.web.support.WebUtils;
import org.joda.time.DateTime;
import org.joda.time.Duration;
import org.joda.time.format.DateTimeFormatter;
import org.joda.time.format.ISODateTimeFormat;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.webflow.execution.RequestContext;

/**
 * Checks whether the user's password is about to expire and displays a view
 * that prompts for password change if the expiration date is below a warning
 * threshold.
 *
 * @author Marvin S. Addison
 * @version $Revision$
 *
 */
public class CheckImpendingPasswordExpirationAction
    extends AbstractLdapQueryAction {

    /** Password expiration date attribute. */
    private static final String PASSWORD_EXPDATE = "passwordExpirationDate";

    /** Person type attribute. */
    private static final String PERSON_TYPE = "personType";

    /** Set of attributes to query. */
    private static final Set<String> ATTRIBUTES = new HashSet<String>(Arrays.asList(PASSWORD_EXPDATE, PERSON_TYPE));

    /** LDAP search filter. */
    private static final String QUERY_TEMPLATE = "(authId=%s)";

    /** Date formatter. */
    private static final DateTimeFormatter FORMATTER = ISODateTimeFormat.dateHourMinuteSecond();

    /** One day duration. */
    private static final Duration ONE_DAY = Duration.standardDays(1);

    /** Two day duration. */
    private static final Duration TWO_DAYS = Duration.standardDays(2);

    /** TGT cookie generator */
    private CookieRetrievingCookieGenerator ticketGrantingTicketCookieGenerator;

    /** Global instance of CAS. */
    private CentralAuthenticationService centralAuthenticationService;

    /** Threshold warning period below which warnings are displayed. */
    private Duration warningPeriod;

    /** URL of password change application for VT constituents. */
    private String passwordChangeUrl;

    /** URL of password change application for guests. */
    private String guestPasswordChangeUrl;


    /**
     * @param url URL to password change application for VT constituents.
     */
    @Required
    public void setPasswordChangeUrl(final String url) {
        this.passwordChangeUrl = url;
    }


    /**
     * @param url URL to password change application for guests.
     */
    @Required
    public void setGuestPasswordChangeUrl(final String url) {
        this.guestPasswordChangeUrl = url;
    }


    /**
     * Sets the TGT cookie generator.
     *
     * @param generator Cookie generator.
     */
    @Required
    public void setTicketGrantingTicketCookieGenerator(final CookieRetrievingCookieGenerator generator) {
        this.ticketGrantingTicketCookieGenerator = generator;
    }


    /**
     * Sets the CAS instance to support dealing with TGT in cookies.
     * @param cas Global CAS instance.
     */
    @Required
    public void setCentralAuthenticationService(final CentralAuthenticationService cas) {
        this.centralAuthenticationService = cas;
    }


    /**
     * Sets the password warning threshold period below which warnings are displayed.
     *
     * @param  days  Threshold warning period in days.
     */
    @Required
    public void setWarningPeriod(final int days) {
        this.warningPeriod = Duration.standardDays(days);
    }


    /** {@inheritDoc} */
    @Override
    protected boolean preHandleContext(final RequestContext context, final String username) {
        // Must set the TGT cookie to signal successful authentication
        final String tgtId = WebUtils.getTicketGrantingTicketId(context);
        final String tgtIdFromCookie = (String) context.getFlowScope().get("ticketGrantingTicketId");
        if (tgtId != null) {
            this.ticketGrantingTicketCookieGenerator.addCookie(
                    WebUtils.getHttpServletRequest(context),
                    WebUtils.getHttpServletResponse(context),
                    tgtId);
        }

        if (tgtIdFromCookie != null && !tgtId.equals(tgtIdFromCookie)) {
            // If a previous cookie was provided and a new one was issued,
            // the old should be destroyed
            this.centralAuthenticationService.destroyTicketGrantingTicket(tgtIdFromCookie);
        }
        return true;
    }


    /** {@inheritDoc} */
    @Override
    protected void postHandleContext(
        final RequestContext context,
        final String username,
        final Map<String, Object> resultMap) {

        if (resultMap.isEmpty()) {
            return;
        }
        final String personType = (String) resultMap.get(PERSON_TYPE);
        final StringBuilder url = new StringBuilder();
        if (personType.contains("Guest")) {
            url.append(guestPasswordChangeUrl).append("?guestId=")
                    .append(username);
        } else {
            url.append(passwordChangeUrl).append("?pid=").append(username);
        }
        final String service = ProtocolParameterAuthority.getServiceId(context);
        if (service != null) {
            url.append("&return_to=").append(UriEncoder.encode(service));
        }
        context.getRequestScope().put("passwordChangeUrl", url.toString());
    }


    /** {@inheritDoc} */
    protected Set<String> getAttributes() {
        return ATTRIBUTES;
    }


    /** {@inheritDoc} */
    protected String getQuery(final String username) {
        return String.format(QUERY_TEMPLATE, username);
    }


    /** {@inheritDoc} */
    protected boolean convertResult(final RequestContext context, final Map<String, Object> resultMap) {
        final DateTime expirationDate = FORMATTER.parseDateTime((String) resultMap.get(PASSWORD_EXPDATE));
        final Duration ttl = new Duration(DateTime.now(), expirationDate);
        if (ttl.isShorterThan(warningPeriod)) {
            context.getRequestScope().put("expirationDate", expirationDate);
            if (ttl.isShorterThan(ONE_DAY)) {
                context.getRequestScope().put("ttl", "less than a day");
            } else if (ttl.isShorterThan(TWO_DAYS)) {
                context.getRequestScope().put("ttl", "less than 2 days");
            } else {
                context.getRequestScope().put("ttl", ttl.getStandardDays() + " days");
            }
            return true;
        }
        return false;
    }


    /** {@inheritDoc} */
    protected void logResult(final String authId, final String result) {
        if (YES.equals(result)) {
            logger.debug("Displaying password expiration warning for " + authId);
        }
    }

}
