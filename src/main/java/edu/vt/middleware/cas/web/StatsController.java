/*
  $Id$

  Copyright (C) 2009 Virginia Tech.
  All rights reserved.

  SEE LICENSE FOR MORE INFORMATION

  Author:  Middleware
  Email:   middleware@vt.edu
  Version: $Revision$
  Updated: $Date$
*/
package edu.vt.middleware.cas.web;

import java.net.InetAddress;
import java.util.Date;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * View for rendering CAS statistics.
 *
 * @author Middleware
 * @version $Revision$
 *
 */
@Controller
@RequestMapping("/secure/stats")
public class StatsController {

    /** Controller view name. */
    private static final String VIEW_NAME = "casStatsView";

    /** Milliseconds per second. */
    private static final int MS_PER_SEC = 1000;

    /** Hours in a day. */
    private static final int HR_PER_DAY = 24;

    /** Seconds per hour. */
    private static final int SEC_PER_HR = 3600;

    /** Minutes per hour. */
    private static final int MIN_PER_HR = 60;

    /** Seconds per minute. */
    private static final int SEC_PER_MIN = 60;

    /** 1 kilobyte. */
    private static final int KB = 1024;

    /** Logger instance. */
    private final Logger logger = LoggerFactory.getLogger(getClass());

    /** Initialized at application startup. */
    private final Date startTime = new Date();


    /**
     * Provides the view for the /stats/ URI.
     *
     * @param  model  Model used for parameterizing view.
     *
     * @return  View name.
     *
     * @throws  Exception  On errors.
     */
    @RequestMapping(value = "/", method = RequestMethod.GET)
    public String index(final Model model) throws Exception {
        model.addAttribute("hostname", InetAddress.getLocalHost().getHostName());
        model.addAttribute("startTime", startTime);
        model.addAttribute("upTime", calculateUpTime(startTime));
        final long freeMem = Runtime.getRuntime().freeMemory();
        final long usedMem = Runtime.getRuntime().totalMemory() - freeMem;
        model.addAttribute("freeMemory", freeMem / KB / KB);
        model.addAttribute("usedMemory", usedMem / KB / KB);
        return VIEW_NAME;
    }


    /**
     * Calculates an uptime string from the given date.
     *
     * @param  start  Uptime start date.
     *
     * @return Uptime duration string of the form
     * [D days] [H hours] [M minutes] S seconds.
     */
    private static String calculateUpTime(final Date start) {
        final StringBuilder sb = new StringBuilder();
        int uptime = (int) ((System.currentTimeMillis() - start.getTime()) / MS_PER_SEC);
        final int d = uptime / HR_PER_DAY / SEC_PER_HR;
        if (d > 0) {
            sb.append(d).append(" days ");
        }
        uptime = uptime % (HR_PER_DAY * SEC_PER_HR);
        final int h = uptime / SEC_PER_HR;
        if (h > 0) {
            sb.append(h).append(" hours ");
        }
        uptime = uptime % SEC_PER_HR;
        final int m = uptime / MIN_PER_HR;
        if (m > 0) {
            sb.append(m).append(" minutes ");
        }
        final int s = uptime % SEC_PER_MIN;
        sb.append(s).append(" seconds");
        return sb.toString();
    }
}
