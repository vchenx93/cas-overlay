/*
  $Id: $

  Copyright (C) 2012 Virginia Tech.
  All rights reserved.

  SEE LICENSE FOR MORE INFORMATION

  Author:  Middleware Services
  Email:   middleware@vt.edu
  Version: $Revision: $
  Updated: $Date: $
*/
package edu.vt.middleware.cas.web.tags;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.tagext.Tag;
import javax.servlet.jsp.tagext.TagSupport;

import edu.vt.middleware.cas.web.util.UrlBuilder;
import org.springframework.util.Assert;
import org.springframework.webflow.execution.RequestContext;

/**
 * JSP tag that understands CAS protocol parameters and properly constructs
 * URLs with supported parameters provided in the request while omitting
 * everything else.  Constructed URLs are assigned to variables in page scope.
 *
 * @author Middleware Services
 * @version $Revision: $
 */
public class CasUrlTag extends TagSupport {
    /** Name of variable exported to page scope containing constructed URL. */
    private String var;

    /** URL to some CAS resource. */
    private String value;

    /** Flag to control handling of URLs that begin with '/'. */
    private boolean contextRelative = true;


    /**
     * Sets the name of variable exported to page scope.
     *
     * @param var Variable to hold constructed URL.
     */
    public void setVar(final String var) {
        this.var = var;
    }


    /**
     * Sets the base URL.
     *
     * @param value Relative or absolute URL to CAS resource.
     */
    public void setValue(final String value) {
        this.value = value;
    }


    /**
     * Sets a flag that determines how URLs beginning with '/' are treated.
     * By default, any URL beginning with '/' is treated as a context-relative
     * URL and is prepended with context path.  Set to false to disable this
     * behavior.
     *
     * @param defaultHandling True to enable default behavior, false to prevent
     *                        prepending context path to URLs beginning with
     *                        '/'.
     */
    public void setContextRelative(final boolean defaultHandling) {
        this.contextRelative = defaultHandling;
    }


    /**
     * Constructs a URL to CAS resource with protocol parameters intact.
     * {@link edu.vt.middleware.cas.web.util.ProtocolParameterAuthority} is used
     * to extract parameters from the current request of the JSP execution
     * context.
     * <p>
     * Relative URLs that begin with '/' are treated as context-relative
     * URLs and the context path of the servlet context is prepended to the
     * resultant vaule.
     *
     * @return <code>Tag.SKIP_BODY</code>
     *
     * @throws JspException On JSP execution errors.
     */
    @Override
    public int doStartTag() throws JspException {
        Assert.hasText(var);
        String newUrl = value;
        final RequestContext context = (RequestContext) pageContext.getRequest().getAttribute("flowRequestContext");
        if (context != null) {
            newUrl = UrlBuilder.build(value, context);
        }
        if (contextRelative && newUrl.indexOf('/') == 0) {
            newUrl = pageContext.getServletContext().getContextPath() + newUrl;
        }
        pageContext.setAttribute(var, newUrl);
        return Tag.SKIP_BODY;
    }
}
