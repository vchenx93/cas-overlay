/*
  $Id$

  Copyright (C) 2009 Virginia Tech.
  All rights reserved.

  SEE LICENSE FOR MORE INFORMATION

  Author:  Middleware
  Email:   middleware@vt.edu
  Version: $Revision$
  Updated: $Date$
*/
package edu.vt.middleware.cas.web.flow;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.webflow.core.FlowException;
import org.springframework.webflow.mvc.servlet.AbstractFlowHandler;

/**
 * Simple {@link FlowHandler} that does not provide special handling for
 * {@link NoSuchFlowExecutionException}s.
 *
 * @author Marvin S. Addison
 * @version $Revision$
 *
 */
public class NoSuchFlowHandler extends AbstractFlowHandler {
    /** {@inheritDoc} */
    public String handleException(
            final FlowException e,
            final HttpServletRequest request,
            final HttpServletResponse response) {
        throw e;
    }

}
