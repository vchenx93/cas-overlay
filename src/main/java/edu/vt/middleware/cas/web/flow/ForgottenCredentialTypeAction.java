/*
  $Id$

  Copyright (C) 2011 Virginia Tech.
  All rights reserved.

  SEE LICENSE FOR MORE INFORMATION

  Author:  Middleware
  Email:   middleware@vt.edu
  Version: $Revision$
  Updated: $Date$
*/
package edu.vt.middleware.cas.web.flow;

import org.springframework.webflow.execution.RequestContext;

/**
 * Webflow action that processes the type of forgotten credential chosen on
 * the "select forgotten credential type" form.
 *
 * @author Marvin S. Addison
 * @version $Revision$
 *
 */
public class ForgottenCredentialTypeAction
    extends AbstractForgottenCredentialAction {

    /** Forgotten username. */
    public static final String USERNAME_TYPE = "username";

    /** Forgotten password. */
    public static final String PASSWORD_TYPE = "password";


    /**
     * Process the forgotten credential type.
     *
     * @param  context  SWF request context.
     *
     * @return  String result indicating next action.
     */
    public String processCredentialType(final RequestContext context) {
        final String type = context.getRequestParameters().get("type");

        if (USERNAME_TYPE.equals(type)) {
            return USERNAME_TYPE;
        } else if (PASSWORD_TYPE.equals(type)) {
            return PASSWORD_TYPE;
        }
        // Don't perform validation on first execution
        if (context.getFlowExecutionContext().getKey() != null) {
            context.getMessageContext().addMessage(
                newErrorMsg(null, "iforgot.error.type"));
        }
        return UNKNOWN;
    }
}
