/*
  $Id$

  Copyright (C) 2011 Virginia Tech.
  All rights reserved.

  SEE LICENSE FOR MORE INFORMATION

  Author:  Middleware
  Email:   middleware@vt.edu
  Version: $Revision$
  Updated: $Date$
*/
package edu.vt.middleware.cas.web.flow;

import org.springframework.binding.message.MessageBuilder;
import org.springframework.binding.message.MessageResolver;

/**
 * Base class for all forgotten credential actions.
 *
 * @author Marvin S. Addison
 * @version $Revision$
 *
 */
public abstract class AbstractForgottenCredentialAction {

    /** Unknown input type. */
    public static final String UNKNOWN = "unknown";

    /** Error result state. */
    public static final String ERROR = "error";


    /**
     * Creates a new error message.
     *
     * @param  source  Error source.
     * @param  code Error message code.
     * @param  args  Error message arguments.
     *
     * @return  Constructed message.
     */
    protected MessageResolver newErrorMsg(
            final String source, final String code, final Object ... args) {
        return new MessageBuilder().error()
            .source(source).code(code).args(args).build();
    }


    /**
     * Creates a new error message.
     *
     * @param  source  Error source.
     * @param  code Error message code.
     *
     * @return  Constructed message.
     */
    protected MessageResolver newErrorMsg(
            final String source, final String code) {
        return new MessageBuilder().error().source(source).code(code).build();
    }
}
