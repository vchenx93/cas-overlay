/*
  $Id: $

  Copyright (C) 2012 Virginia Tech.
  All rights reserved.

  SEE LICENSE FOR MORE INFORMATION

  Author:  Middleware Services
  Email:   middleware@vt.edu
  Version: $Revision: $
  Updated: $Date: $
*/
package edu.vt.middleware.cas.web.flow;

import javax.validation.constraints.NotNull;

import org.jasig.cas.authentication.principal.Service;
import org.jasig.cas.services.RegisteredService;
import org.jasig.cas.services.ServicesManager;
import org.jasig.cas.services.UnauthorizedServiceException;
import org.jasig.cas.web.support.WebUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.webflow.action.AbstractAction;
import org.springframework.webflow.execution.Event;
import org.springframework.webflow.execution.RequestContext;

/**
 * Interrogates the {@link org.jasig.cas.services.ServicesManager} for a registered service
 * given a service identifier and stores the resulting {@link org.jasig.cas.services.RegisteredService}
 * in flow scope under the key "registeredService".
 * If the ServicesManager cannot find a matching service, a
 * {@link org.jasig.cas.services.UnauthorizedServiceException} is raised.
 *
 * @author Middleware Services
 * @version $Revision: $
 */
public class LookupRegisteredServiceAction extends AbstractAction {
    /** Key name under which a retrieved registered service is placed in flow scope. */
    public static final String FLOW_SCOPE_KEY = "registeredService";

    /** Service manager. */
    @NotNull
    private final ServicesManager servicesManager;

    /** Logger instance. */
    private final Logger logger = LoggerFactory.getLogger(this.getClass());


    /**
     * Creates a new instance that uses the given service manager for lookups.
     *
     * @param  manager  Service manager.
     */
    public LookupRegisteredServiceAction(final ServicesManager manager) {
        this.servicesManager = manager;
    }

    /** {@inheritDoc} */
    protected Event doExecute(final RequestContext context) throws Exception {
        final Service service = WebUtils.getService(context);
        if (service == null) {
            return success();
        }

        final RegisteredService registered = this.servicesManager.findServiceBy(service);
        if (registered == null) {
            logger.warn("{} is not authorized to use CAS.", service);
            throw new UnauthorizedServiceException();
        } else if (!registered.isEnabled()) {
            logger.warn("Found registered service {} for {} but it is disabled.", registered.getServiceId(), service);
            throw new UnauthorizedServiceException();
        }
        context.getFlowScope().put("registeredService", registered);
        return success();
    }
}
