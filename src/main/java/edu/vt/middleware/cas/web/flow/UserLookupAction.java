/*
  $Id$

  Copyright (C) 2011 Virginia Tech.
  All rights reserved.

  SEE LICENSE FOR MORE INFORMATION

  Author:  Middleware
  Email:   middleware@vt.edu
  Version: $Revision$
  Updated: $Date$
*/
package edu.vt.middleware.cas.web.flow;

import javax.naming.directory.Attribute;

import edu.vt.middleware.cas.web.util.ProtocolParameterAuthority;
import edu.vt.middleware.cas.web.util.UriEncoder;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.ldap.core.ContextMapper;
import org.springframework.ldap.core.ContextSource;
import org.springframework.ldap.core.DirContextAdapter;
import org.springframework.ldap.core.LdapTemplate;
import org.springframework.webflow.execution.RequestContext;

/**
 * Looks up a user in the LDAP directory in support of guided help for
 * forgotten passwords.
 *
 * @author Marvin S. Addison
 * @version $Revision$
 *
 */
public class UserLookupAction extends AbstractForgottenCredentialAction {

    /** Username found. */
    public static final String FOUND = "found";

    /** Guest not found. */
    public static final String GUEST_NOT_FOUND = "guest_not_found";

    /** PID not found. */
    public static final String PID_NOT_FOUND = "pid_not_found";

    /** LDAP search base. */
    private static final String SEARCH_BASE = "ou=People,dc=vt,dc=edu";

    /** Authentication ID attribute. */
    private static final String AUTH_ID = "authId";

    /** Person type attribute. */
    private static final String PERSON_TYPE = "personType";

    /** Display name attribute. */
    private static final String DISPLAY_NAME = "displayName";

    /** Logger instance. */
    protected final Log logger = LogFactory.getLog(getClass());

    /** ED-ID context source. */
    private ContextSource edIdContextSource;

    /** ED-Lite context source. */
    private ContextSource edLiteContextSource;


    /** Maps the value of an ldap entry onto a User object. */
    private final ContextMapper resultMapper = new ContextMapper() {
        public Object mapFromContext(final Object o) {
            final DirContextAdapter adapter = (DirContextAdapter) o;
            return new User(
                getAttribute(adapter, AUTH_ID),
                getAttribute(adapter, PERSON_TYPE),
                getAttribute(adapter, DISPLAY_NAME));
        }
    };

    /** URL of password reset application for VT constituents. */
    private String passwordResetUrl;

    /** URL of password reset application for guests. */
    private String guestPasswordResetUrl;


    /**
     * @param url URL to password reset application for VT constituents.
     */
    @Required
    public void setPasswordResetUrl(final String url) {
        this.passwordResetUrl = url;
    }


    /**
     * @param url URL to password reset application for guests.
     */
    @Required
    public void setGuestPasswordResetUrl(final String url) {
        this.guestPasswordResetUrl = url;
    }


    /**
     * @param  contextSource  Sets the context source to be used for ED-ID
     * queries.
     */
    @Required
    public void setEdIdContextSource(final ContextSource contextSource) {
        this.edIdContextSource = contextSource;
    }


    /**
     * @param  contextSource  Sets the context source to be used for ED-Lite
     * queries.
     */
    @Required
    public void setEdLiteContextSource(final ContextSource contextSource) {
        this.edLiteContextSource = contextSource;
    }


    /**
     * Look up a user in the LDAP directory.
     *
     * @param  context  SWF request context.
     * @param  username  User to look up.
     *
     * @return  FOUND if user is found, GUEST_NOT_FOUND if a guest user is not
     * found, PID_NOT_FOUND if uupid not found, otherwise ERROR.
     */
    public String lookupUser(
            final RequestContext context, final String username) {
        if (username == null || username.isEmpty()) {
            context.getMessageContext().addMessage(newErrorMsg(null, "iforgot.error.userRequired"));
            return ERROR;
        }
        String result;
        try {
            final User user;
            if (username.contains("@")) {
                user = searchForUser(username);
            } else {
                user = new User(username, "Virginia Tech Person");
            }
            final StringBuilder url = new StringBuilder();
            if (user.isGuest()) {
                if (user.getDisplayName() == null) {
                    user.setDisplayName(user.getUsername());
                }
                url.append(guestPasswordResetUrl).append("?guestId=");
                url.append(UriEncoder.encode(user.getUsername()));
            } else {
                user.setDisplayName(user.getUsername());
                url.append(passwordResetUrl).append("?pid=");
                url.append(user.getUsername());
            }
            final String service = ProtocolParameterAuthority.getServiceId(context);
            if (service != null) {
                url.append("&return_to=").append(UriEncoder.encode(service));
            }
            context.getRequestScope().put("user", user);
            context.getRequestScope().put("passwordResetUrl", url.toString());
            result = FOUND;
        } catch (EmptyResultDataAccessException empty) {
            context.getRequestScope().put("username", username);
            result = username.contains("@") ? GUEST_NOT_FOUND : PID_NOT_FOUND;
        }
        return result;
    }


    /**
     * Gets the value of then given attribute name from an LDAP search result.
     *
     * @param  adapter  Search result context.
     *
     * @param  attrName  Attribute name.
     *
     * @return  Attribute value or null if attribute not found or cannot be
     * retrieved.
     */
    private String getAttribute(
            final DirContextAdapter adapter, final String attrName) {
        final Attribute attr = adapter.getAttributes().get(attrName);
        try {
            return attr != null ? (String) attr.get() : null;
        } catch (Exception e) {
            logger.error("Error fetching attribute " + attrName, e);
            return null;
        }
    }


    /**
     * Executes an LDAP query for the given user.
     *
     * @param  username  Username to search for.
     *
     * @return  User object containing user data if found.
     */
    private User searchForUser(final String username) {
        final String query =
            new StringBuilder(AUTH_ID).append('=').append(username).toString();
        logger.debug("Looking up user " + username);
        return (User) new LdapTemplate(edIdContextSource).searchForObject(
                SEARCH_BASE, query, resultMapper);
    }
}
