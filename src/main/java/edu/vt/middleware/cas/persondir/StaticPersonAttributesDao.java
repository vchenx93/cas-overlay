/*
  $Id$

  Copyright (C) 2009 Virginia Tech.
  All rights reserved.

  SEE LICENSE FOR MORE INFORMATION

  Author:  Middleware
  Email:   middleware@vt.edu
  Version: $Revision$
  Updated: $Date$
*/
package edu.vt.middleware.cas.persondir;

import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.jasig.services.persondir.IPersonAttributes;
import org.jasig.services.persondir.support
    .AbstractDefaultAttributePersonAttributeDao;
import org.jasig.services.persondir.support.NamedPersonImpl;

/**
 * Provides a map of static attributes for a given query where the person name
 * is derived from the given query.
 *
 * @author Middleware
 * @version $Revision$
 *
 */
public class StaticPersonAttributesDao extends
        AbstractDefaultAttributePersonAttributeDao {

    /** Static attribute mapping */
    private Map<String, List<Object>> backingMap;


    /**
     * Sets the map of static attributes.
     *
     * @param  map  Map of static attributes.
     */
    public void setBackingMap(final Map<String, List<Object>> map) {
        this.backingMap = map;
    }


    /** {@inheritDoc} */
    public Set<String> getAvailableQueryAttributes() {
        return null;
    }

    /** {@inheritDoc} */
    public Set<IPersonAttributes> getPeopleWithMultivaluedAttributes(
            final Map<String, List<Object>> query) {
        final String uid = this.getUsernameAttributeProvider()
            .getUsernameFromQuery(query);
        if (uid == null) {
            logger.warn("Cannot determine username from query.");
        }
        final IPersonAttributes result = new NamedPersonImpl(uid, backingMap);
        return Collections.singleton(result);
    }

    /** {@inheritDoc} */
    public Set<String> getPossibleUserAttributeNames() {
        return Collections.unmodifiableSet(backingMap.keySet());
    }

}
