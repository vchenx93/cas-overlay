/*
  $Id$

  Copyright (C) 2009 Virginia Tech.
  All rights reserved.

  SEE LICENSE FOR MORE INFORMATION

  Author:  Middleware
  Email:   middleware@vt.edu
  Version: $Revision$
  Updated: $Date$
*/
package edu.vt.middleware.cas.audit;

import java.util.List;
import javax.validation.constraints.NotNull;

import com.github.inspektr.common.spi.PrincipalResolver;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.aspectj.lang.JoinPoint;

import org.jasig.cas.authentication.principal.Credentials;
import org.jasig.cas.authentication.principal.CredentialsToPrincipalResolver;
import org.jasig.cas.authentication.principal.Principal;
import org.jasig.cas.util.AopUtils;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;

/**
 * Resolves a principal from a ticket ID or credential.
 *
 * @author Middleware
 * @version $Revision$
 *
 */
public class TicketOrCredentialPrincipalResolver implements PrincipalResolver {

    /** Name of CentralAuthenticationService#destroyTicketGrantingTicket method. */
    private static final String DESTROY_TGT = "destroyTicketGrantingTicket";

    /** Name of CentralAuthenticationService#validateServiceTicket method. */
    private static final String VALIDATE_ST = "validateServiceTicket";

    /** Logger instance */
    private final Log logger = LogFactory.getLog(getClass());

    /** Resolvers that can transform a credential to a principal */
    @NotNull
    private List<CredentialsToPrincipalResolver> cpResolvers;


    /**
     * @param resolvers List of credentials-to-principal resolvers.
     */
    public void setCredentialsToPrincipalResolvers(
            final List<CredentialsToPrincipalResolver> resolvers) {
        this.cpResolvers = resolvers;
    }


    /** {@inheritDoc} */
    public String resolveFrom(final JoinPoint joinPoint, final Object retVal) {
        return resolveFromInternal(AopUtils.unWrapJoinPoint(joinPoint));
    }


    /** {@inheritDoc} */
    public String resolveFrom(
            final JoinPoint joinPoint, final Exception retVal) {
        return resolveFromInternal(AopUtils.unWrapJoinPoint(joinPoint));
    }


    /** {@inheritDoc} */
    public String resolve() {
        return UNKNOWN_USER;
    }


    /**
     * Resolves a principal from the given join point.
     *
     * @param joinPoint AOP join point.
     *
     * @return Resolved principal name/ID.
     */
    protected String resolveFromInternal(final JoinPoint joinPoint) {
        final String methodName = joinPoint.getSignature().getName();
        String principalId = UNKNOWN_USER;
        final Object arg1 = joinPoint.getArgs()[0];
        if (DESTROY_TGT.equals(methodName)) {
            return UNKNOWN_USER;
        } else if (VALIDATE_ST.equals(methodName)) {
            // TODO: look up TGT and set the principal ID to TGT ID
            // At present there is no facility to accomplish this
            // Matching up TGT to ST for validation would facilitate queries
            // such as "Show me all the services accessed with TGT X"
            principalId = UNKNOWN_USER;
        } else if (arg1 instanceof String) {
            // Treat the ticket ID as the principal
            principalId = (String) arg1;
        } else if (arg1 instanceof Credentials) {
            final Credentials credentials = (Credentials) arg1;
            principalId = credentials.toString();
            for (CredentialsToPrincipalResolver r : cpResolvers) {
                if (r.supports(credentials)) {
                    try {
                        final Principal principal =
                            r.resolvePrincipal(credentials);
                        if (principal != null) {
                            principalId = principal.getId();
                            break;
                        }
                    } catch (Exception e) {
                        if (logger.isDebugEnabled()) {
                            logger.warn("Failed resolving principal ", e);
                        } else {
                            logger.warn("Failed resolving principal: " + e);
                        }
                    }
                }
            }
        } else {
            final SecurityContext securityContext =
                SecurityContextHolder.getContext();
            if (securityContext != null) {
                final Authentication authentication =
                    securityContext.getAuthentication();
                if (authentication != null) {
                    principalId = authentication.getName();
                }
            }
        }
        return principalId;
    }
}
