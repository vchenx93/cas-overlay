/*
  $Id$

  Copyright (C) 2009 Virginia Tech.
  All rights reserved.

  SEE LICENSE FOR MORE INFORMATION

  Author:  Middleware
  Email:   middleware@vt.edu
  Version: $Revision$
  Updated: $Date$
*/
package edu.vt.middleware.cas.audit;

import com.github.inspektr.audit.spi.AuditResourceResolver;

import org.aspectj.lang.JoinPoint;
import org.jasig.cas.authentication.principal.Service;
import org.jasig.cas.util.AopUtils;

/**
 * Generates an array of resource names containing the service ID and service ticket ID
 * for a method with the following signature:
 *
 * <pre>
 *     String generateTicketGrantingTicket(String, Service, ...);
 * </pre>
 *
 * where the first argument is the TGT ID, and the method returns a service ticket ID.
 *
 * @author Middleware
 * @version $Revision$
 *
 */
public class ServiceResourceResolver implements AuditResourceResolver {

    /** {@inheritDoc} */
    public String[] resolveFrom(
            final JoinPoint auditableTarget,
            final Object retval) {
        final JoinPoint point = AopUtils.unWrapJoinPoint(auditableTarget);
        return new String[] {
            ((Service) point.getArgs()[1]).getId(),
            (String) retval,
        };
    }


    /** {@inheritDoc} */
    public String[] resolveFrom(
            final JoinPoint auditableTarget,
            final Exception exception) {
        final JoinPoint point = AopUtils.unWrapJoinPoint(auditableTarget);
        return new String[] { ((Service) point.getArgs()[1]).getId() };
    }
}
