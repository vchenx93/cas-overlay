/*
  $Id$

  Copyright (C) 2013 Virginia Tech.
  All rights reserved.

  SEE LICENSE FOR MORE INFORMATION

  Author:  Middleware
  Email:   middleware@vt.edu
  Version: $Revision$
  Updated: $Date$
*/
package edu.vt.middleware.cas.audit;

import com.github.inspektr.audit.AuditActionContext;
import com.github.inspektr.audit.AuditTrailManager;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormatter;
import org.joda.time.format.ISODateTimeFormat;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Inspektr audit trail manager that writes audit data to a file using an slf4j
 * logger in a more compact format better suited to tooling. The format of the message follows:
 *
 * <pre>
 * WHEN|WHAT|WHO|ACTION|SOURCE_IP
 * </pre>
 *
 * where the WHEN date is in ISO8601 format.
 * <p>
 * The logger category used to log messages is AUDIT. The logger should be
 * configured to print the logger message exclusively; for example the log4j
 * pattern would be <code>%m%n</code>. Audit entries are logged at the INFO level.
 *
 * @author  Middleware Services
 * @version  $Revision$
 */
public class CompactSlf4jAuditTrailManager implements AuditTrailManager {

    /** Logger category for audit trail messages, {@value}. */
    public static final String CATEGORY = "AUDIT";

    /** Logger that prints audit trail messages. */
    private final Logger logger = LoggerFactory.getLogger(CATEGORY);

    /** Date formatter. */
    private final DateTimeFormatter formatter = ISODateTimeFormat.dateHourMinuteSecondMillis();


    /** {@inheritDoc} */
    public void record(final AuditActionContext context) {
        final StringBuilder sb = new StringBuilder(formatter.print(new DateTime(context.getWhenActionWasPerformed())));
        sb.append('|').append(context.getResourceOperatedUpon());
        sb.append('|').append(context.getPrincipal());
        sb.append('|').append(context.getActionPerformed());
        sb.append('|').append(context.getClientIpAddress());
        logger.info(sb.toString());
    }
}
