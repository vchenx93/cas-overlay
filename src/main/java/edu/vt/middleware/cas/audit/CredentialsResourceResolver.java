/*
  $Id$

  Copyright (C) 2009 Virginia Tech.
  All rights reserved.

  SEE LICENSE FOR MORE INFORMATION

  Author:  Middleware
  Email:   middleware@vt.edu
  Version: $Revision$
  Updated: $Date$
*/
package edu.vt.middleware.cas.audit;

import com.github.inspektr.audit.spi.AuditResourceResolver;

import org.aspectj.lang.JoinPoint;

import org.jasig.cas.adaptors.x509.authentication.principal
    .X509CertificateCredentials;
import org.jasig.cas.authentication.principal.Credentials;
import org.jasig.cas.authentication.principal.UsernamePasswordCredentials;
import org.jasig.cas.util.AopUtils;

/**
 * Resolves audit data about a credential.
 *
 * @author Middleware
 * @version $Revision$
 *
 */
public class CredentialsResourceResolver implements AuditResourceResolver {

    /** {@inheritDoc} */
    public String[] resolveFrom(
            final JoinPoint auditableTarget,
            final Object retval) {

        return resolveInternal(
                AopUtils.unWrapJoinPoint(auditableTarget).getArgs()[0]);
    }


    /** {@inheritDoc} */
    public String[] resolveFrom(
            final JoinPoint auditableTarget,
            final Exception exception) {

        return resolveInternal(
                AopUtils.unWrapJoinPoint(auditableTarget).getArgs()[0]);
    }


    /**
     * Resolves credential audit data from the given argument,
     * which must be some sort of {@link Credential}.
     *
     * @param arg The {@link Credential} supplied by the auditable resource.
     *
     * @return A string identifying the type of credential supplied.
     */
    protected String[] resolveInternal(final Object arg) {
        final Credentials credentials = (Credentials) arg;
        String id = credentials.toString();
        if (credentials instanceof UsernamePasswordCredentials) {
            id = ((UsernamePasswordCredentials) credentials).getUsername();
        } else if (credentials instanceof X509CertificateCredentials) {
            final X509CertificateCredentials certCredentials =
                (X509CertificateCredentials) credentials;
            // Must perform a null check on the certificate
            // since it will be null when X509CredentialsAuthenticationHandler
            // certificate validation fails
            if (certCredentials.getCertificate() != null) {
                id = certCredentials.getCertificate().getSubjectDN().getName();
            } else {
                id = "unknown X.509 certificiate";
            }
        }
        return new String[] { "Supplied credentials: " + id };
    }
}
