/*
  $Id$

  Copyright (C) 2012 Virginia Tech.
  All rights reserved.

  SEE LICENSE FOR MORE INFORMATION

  Author:  Middleware Services
  Email:   middleware@vt.edu
  Version: $Revision$
  Updated: $Date$
*/
package edu.vt.middleware.cas.util;

import java.security.cert.Certificate;
import java.security.cert.X509Certificate;

import edu.vt.middleware.crypt.util.CryptReader;

/**
 * Helper class for convenient certificate handling operations.
 *
 * @author Middleware Services
 * @version $Revision$
 */
public final class X509Helper {

    /** Private contstructor of utility class. */
    private X509Helper() {}

    /**
     * Gets the X.509 certificates from the PEM-encoded file at the given
     * classpath location.
     *
     * @param  classpath  Path to certificate PEM file.  May contain one or
     * more certificates.
     *
     * @return  All certificates read from file.
     */
    public static X509Certificate[] readCertificates(final String classpath) {
        Certificate[] certs = null;
        try {
            certs = CryptReader.readCertificateChain(X509Helper.class.getResourceAsStream(classpath));
        } catch (Exception e) {
            throw new RuntimeException(
                    "Error reading certificates at " + classpath, e);
        }
        final X509Certificate[] x509Certs = new X509Certificate[certs.length];
        System.arraycopy(certs, 0, x509Certs, 0, certs.length);
        return x509Certs;
    }


    /**
     * Gets the first non-CA certificate among the given certificates, which in practice constitute a cert chain.
     *
     * @param certificates One or more certificates to examine.
     *
     * @return First non-CA certificate among given certs or null if there is no non-CA certificate.
     */
    public static X509Certificate getUserCertificate(final X509Certificate ... certificates) {
        if (certificates != null) {
            for (int i = 0; i < certificates.length; i++) {
                final X509Certificate certificate = certificates[i];
                // getBasicConstraints returns -1 for non-CA certificates
                if (certificate.getBasicConstraints() == -1) {
                    return certificate;
                }
            }
        }
        return null;
    }
}
