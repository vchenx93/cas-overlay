/*
  $Id$

  Copyright (C) 2009 Virginia Tech.
  All rights reserved.

  SEE LICENSE FOR MORE INFORMATION

  Author:  Middleware
  Email:   middleware@vt.edu
  Version: $Revision$
  Updated: $Date$
*/
package edu.vt.middleware.cas.authentication.metadata;

import java.util.Set;
import javax.validation.constraints.NotNull;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.jasig.cas.authentication.Authentication;
import org.jasig.cas.authentication.AuthenticationMetaDataPopulator;
import org.jasig.cas.authentication.principal.Credentials;

/**
 * Adds a level of assurance value to the authentication based on credential type used to authenticate.
 *
 * @author Middleware
 * @version $Revision$
 *
 */
public class LevelOfAssuranceMetaDataPopulator implements AuthenticationMetaDataPopulator {
    /** Logger instance */
    private final Log logger = LogFactory.getLog(getClass());

    /** Assurance attribute name. */
    @NotNull
    private String attributeName;

    /** Calculates the LOA value of the credential */
    @NotNull
    private Set<LevelOfAssuranceCalculator> calculators;


    /**
     * Sets the attribute name holding calculated assurance value to be added to authentication attributes.
     *
     * @param name Name of attribute to be added to authentication.
     */
    public void setAttributeName(final String name) {
        this.attributeName = name;
    }


    /**
     * @param set Set of calculators that compute assurance from authentication credentials.
     */
    public void setCalculators(final Set<LevelOfAssuranceCalculator> set) {
        this.calculators = set;
    }


    /** {@inheritDoc} */
    public Authentication populateAttributes(final Authentication authentication, final Credentials credential) {
        String level;
        for (LevelOfAssuranceCalculator calculator : calculators) {
            if (calculator.supports(credential)) {
                level = calculator.calculate(credential);
                logger.debug(String.format("Setting %s to %s", attributeName, level));
                authentication.getAttributes().put(attributeName, level);
            }
        }
        return authentication;
    }
}
