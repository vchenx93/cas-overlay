/*
  $Id$

  Copyright (C) 2009 Virginia Tech.
  All rights reserved.

  SEE LICENSE FOR MORE INFORMATION

  Author:  Middleware
  Email:   middleware@vt.edu
  Version: $Revision$
  Updated: $Date$
*/
package edu.vt.middleware.cas.authentication.principal;

import org.jasig.cas.authentication.principal.Credentials;
import org.jasig.cas.authentication.principal.UsernamePasswordCredentials;

/**
 * Resolves a principal with attributes given username/password credentials.
 *
 * @author Middleware
 * @version $Revision$
 *
 */
public class UsernamePasswordCredentialsToPrincipalResolver extends AbstractCredentialsToPrincipalResolver {

    /** {@inheritDoc} */
    public boolean supports(final Credentials credentials) {
        return credentials instanceof UsernamePasswordCredentials;
    }


    /** {@inheritDoc} */
    public String resolvePrincipalId(final Credentials credentials) {
        return ((UsernamePasswordCredentials) credentials).getUsername();
    }
}
