/*
  $Id$

  Copyright (C) 2009 Virginia Tech.
  All rights reserved.

  SEE LICENSE FOR MORE INFORMATION

  Author:  Middleware
  Email:   middleware@vt.edu
  Version: $Revision$
  Updated: $Date$
*/
package edu.vt.middleware.cas.authentication.metadata;


import javax.validation.constraints.NotNull;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.jasig.cas.authentication.principal.Credentials;
import org.jasig.cas.authentication.principal.UsernamePasswordCredentials;

/**
 * Determines the LOA of an ED username/password credential.
 *
 * @author Middleware
 * @version $Revision$
 *
 */
public final class AuthIdLevelOfAssuranceCalculator implements LevelOfAssuranceCalculator {

    /** Logger instance */
    private final Log logger = LogFactory.getLog(getClass());

    /** LOA value for PID accounts */
    @NotNull
    private String pidAccountLevel;

    /** LOA value for guest accounts */
    @NotNull
    private String guestAccountLevel;


    /**
     * Sets the LOA string for PID accounts.
     *
     * @param loa PID LOA value.
     */
    public void setPidAccountLevel(final String loa) {
        this.pidAccountLevel = loa;
    }


    /**
     * Sets the LOA string for guest accounts.
     *
     * @param loa Guest LOA value.
     */
    public void setGuestAccountLevel(final String loa) {
        this.guestAccountLevel = loa;
    }


    /** {@inheritDoc} */
    public String calculate(final Credentials credentials) {
        if (!supports(credentials)) {
            throw new IllegalArgumentException(credentials + " not supported.");
        }
        logger.debug("Calculating LOA for " + credentials);
        if (isGuest((UsernamePasswordCredentials) credentials)) {
            return guestAccountLevel;
        }
        return pidAccountLevel;
    }


    /** {@inheritDoc} */
    public boolean supports(final Credentials credentials) {
        return UsernamePasswordCredentials.class.isInstance(credentials);
    }


    /**
     * Determines whether the given credential represents a guestId or uupid.
     *
     * @param credentials Username/password credentials.
     * @return True if username is a guestId, false otherwise.
     */
    private boolean isGuest(final UsernamePasswordCredentials credentials) {
        return credentials.getUsername().indexOf('@') > -1;
    }
}
