/*
  $Id$

  Copyright (C) 2009 Virginia Tech.
  All rights reserved.

  SEE LICENSE FOR MORE INFORMATION

  Author:  Middleware
  Email:   middleware@vt.edu
  Version: $Revision$
  Updated: $Date$
*/
package edu.vt.middleware.cas.authentication.metadata;

import java.security.cert.X509Certificate;
import java.util.List;
import java.util.Map;
import javax.validation.constraints.NotNull;

import edu.vt.middleware.cas.util.X509Helper;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.bouncycastle.asn1.x509.PolicyInformation;
import org.cryptacular.x509.ExtensionReader;
import org.jasig.cas.adaptors.x509.authentication.principal.X509CertificateCredentials;
import org.jasig.cas.authentication.principal.Credentials;

/**
 * Determines the LOA of a Virginia Tech Personal Digital (X.509) Certificate.
 *
 * @author Middleware
 * @version $Revision$
 *
 */
public final class PDCLevelOfAssuranceCalculator implements LevelOfAssuranceCalculator {

    /** Logger instance */
    private final Log logger = LogFactory.getLog(getClass());

    /** Default level returned when no mapping is found for given OID. */
    @NotNull
    private String defaultLevel;

    /** Map of certificate LOA OID values to assurance levels. */
    @NotNull
    private Map<String, String> oidLevelMap;


    /**
     * Sets the default LOA to be used when no value is found in the map for a given OID.
     * @param level Default LOA.
     */
    public void setDefaultLevel(final String level) {
        this.defaultLevel = level;
    }


    /**
     * Sets the map of LOA OID values to assurance levels.
     * @param map Map of OID strings to string LOA values.
     */
    public void setOidLevelMap(final Map<String, String> map) {
        this.oidLevelMap = map;
    }


    /** {@inheritDoc} */
    public String calculate(final Credentials credentials) {
        if (!supports(credentials)) {
            throw new IllegalArgumentException(credentials + " not supported.");
        }
        logger.debug("Calculating LOA for " + credentials);
        final X509CertificateCredentials certCredentials = (X509CertificateCredentials) credentials;
        X509Certificate userCert = certCredentials.getCertificate();
        if (userCert == null) {
            userCert = X509Helper.getUserCertificate(certCredentials.getCertificates());
        }
        if (userCert == null) {
            throw new IllegalArgumentException("Cannot find user certificate.");
        }
        final ExtensionReader reader = new ExtensionReader(userCert);
        final List<PolicyInformation> policies = reader.readCertificatePolicies();
        for (String oid : oidLevelMap.keySet()) {
            for (int i = 0; i < policies.size(); i++) {
                if (oid.equals(policies.get(i).getPolicyIdentifier().getId())) {
                    return oidLevelMap.get(oid);
                }
            }
        }
        return defaultLevel;
    }


    /** {@inheritDoc} */
    public boolean supports(final Credentials credentials) {
        return X509CertificateCredentials.class.isInstance(credentials);
    }

}
