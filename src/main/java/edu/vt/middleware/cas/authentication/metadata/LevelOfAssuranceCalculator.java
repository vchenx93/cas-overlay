/*
  $Id$

  Copyright (C) 2009 Virginia Tech.
  All rights reserved.

  SEE LICENSE FOR MORE INFORMATION

  Author:  Middleware
  Email:   middleware@vt.edu
  Version: $Revision$
  Updated: $Date$
*/
package edu.vt.middleware.cas.authentication.metadata;

import org.jasig.cas.authentication.principal.Credentials;

/**
 * Determines the Level of Authentication Assurance of a credential.
 * <p>
 * The URNs used for levels were inspired by section 3 of
 * {@link http://www.oasis-open.org/committees/download.php/28706/
 *sstc-saml-loa-authncontext-profile-draft-01.pdf}.
 *
 * @author Middleware
 * @version $Revision$
 *
 */
public interface LevelOfAssuranceCalculator {

    /**
     * Calculates the LOA value as a contextual string.
     *
     * @param credentials Authentication credential.
     * @return Level of assurance value.
     */
    String calculate(final Credentials credentials);


    /**
     * Determines whether this LOA calculator can determine the LOA from
     * the given type of credentials.
     *
     * @param credentials Credentials from which LOA should be calculated.
     * @return True if given credentials are supported, false otherwise.
     */
    boolean supports(Credentials credentials);
}
