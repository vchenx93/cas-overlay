/*
  $Id$

  Copyright (C) 2009 Virginia Tech.
  All rights reserved.

  SEE LICENSE FOR MORE INFORMATION

  Author:  Middleware
  Email:   middleware@vt.edu
  Version: $Revision$
  Updated: $Date$
*/
package edu.vt.middleware.cas.authentication.principal;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import javax.validation.constraints.NotNull;

import edu.vt.middleware.cas.authentication.metadata.LevelOfAssuranceCalculator;
import net.sf.ehcache.Cache;
import net.sf.ehcache.Element;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.jasig.cas.authentication.principal.Credentials;
import org.jasig.cas.authentication.principal.CredentialsToPrincipalResolver;
import org.jasig.cas.authentication.principal.Principal;
import org.jasig.cas.authentication.principal.SimplePrincipal;
import org.jasig.services.persondir.IPersonAttributeDao;
import org.jasig.services.persondir.IPersonAttributes;

/**
 * Abstract base class for all VT credentials to principal resolvers.
 *
 * @author Middleware
 * @version $Revision$
 *
 */
public abstract class AbstractCredentialsToPrincipalResolver implements CredentialsToPrincipalResolver {

    /** ED LDAP attribute holding the principal name */
    public static final String AUTHID_ATTR = "authId";

    /** Logger instance. */
    protected final Log logger = LogFactory.getLog(this.getClass());

    /** Repository of principal attributes to be retrieved */
    @NotNull
    private IPersonAttributeDao attributeRepository;

    /** Calculates the LOA value of the credential */
    @NotNull
    private Set<LevelOfAssuranceCalculator> calculators;

    /** Name of principal attribute containing LOA value. */
    @NotNull
    private String loaAttributeName;


    /** Cache of recently resolved principals */
    private Cache principalCache;


    /**
     * Sets the name of the principal attribute containing LOA value.
     *
     * @param name Attribute name.
     */
    public void setLoaAttributeName(final String name) {
        this.loaAttributeName = name;
    }


    /**
     * @param set Set of LOA calculators to handle all expected types of credentials.
     */
    public void setCalculators(final Set<LevelOfAssuranceCalculator> set) {
        this.calculators = set;
    }


    /**
     * Sets the EhCache instance to use for caching principals.
     * If none is specified, no caching is performed.
     *
     * @param  cache  EhCache cache instance.
     */
    public void setPrincipalCache(final Cache cache) {
        this.principalCache = cache;
    }


    /**
     * Sets the attribute repository used to resolve principal attributes.
     *
     * @param repository Person directory attribute repository.
     */
    public final void setAttributeRepository(
            final IPersonAttributeDao repository) {
        if (!repository.getPossibleUserAttributeNames().contains(AUTHID_ATTR)) {
            throw new IllegalArgumentException(
                "Attribute repository must contain attribute " + AUTHID_ATTR);
        }
        this.attributeRepository = repository;
    }


    /** {@inheritDoc} */
    public Principal resolvePrincipal(final Credentials credentials) {
        logger.debug("Attempting to resolve a principal...");

        Principal principal = null;
        if (principalCache != null) {
            final Element result = principalCache.get(credentials);
            if (result != null) {
                principal = (Principal) result.getValue();
                logger.debug("Principal " + principal + " found in cache.");
                return principal;
            } else {
                logger.debug("Principal not found in cache.");
            }
        }
        final String id = resolvePrincipalId(credentials);
        if (id == null) {
            logger.warn(
                    "Cannot determine username from credentials.  "
                    + "Returning null for principal.");
            return null;
        }

        final Map<String, Object> attributes = getPersonDirAttributes(id);
        final String authId = getAuthId(attributes);
        if (authId == null) {
            logger.warn(
                    "Cannot determine authId.  Returning null for principal.");
            return null;
        }
        String level;
        for (LevelOfAssuranceCalculator calculator : calculators) {
            if (calculator.supports(credentials)) {
                level = calculator.calculate(credentials);
                logger.debug(String.format("Setting %s to %s", loaAttributeName, level));
                attributes.put(loaAttributeName, level);
            }
        }
        principal = new SimplePrincipal(authId, attributes);
        logger.debug("Created principal " + principal);
        if (principalCache != null) {
            logger.debug(String.format("Caching principal %s with key %s.",
                    principal, credentials));
            principalCache.put(new Element(credentials, principal));
        }
        return principal;
    }


    /**
     * Gets attributes for the given principal ID from person directory.
     *
     * @param id Principal name identifier.
     * @return Mapping of attribute names to values.
     */
    private Map<String, Object> getPersonDirAttributes(final String id) {
        final Map<String, Object> attributes = new HashMap<String, Object>();
        final IPersonAttributes person = this.attributeRepository.getPerson(id);
        if (person != null && person.getAttributes() != null) {
            for (final Map.Entry<String, List<Object>> entry
                    : person.getAttributes().entrySet()) {

                final String key = entry.getKey();
                final List<Object> values = entry.getValue();
                if (values != null) {
                    final Object value = entry.getValue().size() == 1
                        ? entry.getValue().get(0)
                        : entry.getValue();
                    attributes.put(key, value);
                } else {
                    attributes.put(key, null);
                }
            }
        }
        return attributes;
    }


    /**
     * Gets the authId from the directory attributes.
     * @param attributes LDAP attributes.
     * @return Value of authId attribute among attributes or null if
     * not found.
     */
    private String getAuthId(final Map<String, Object> attributes) {
        for (String key : attributes.keySet()) {
            if (AUTHID_ATTR.equals(key)) {
                return (String) attributes.get(key);
            }
        }
        return null;
    }


    /**
     * Resolves the principal ID from the given credentials.
     *
     * @param  credentials  Credentials supplied by user.
     *
     * @return  Principal ID or null on resolution failure.
     */
    protected abstract String resolvePrincipalId(Credentials credentials);
}
