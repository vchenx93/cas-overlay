/*
  $Id$

  Copyright (C) 2013 Virginia Tech.
  All rights reserved.

  SEE LICENSE FOR MORE INFORMATION

  Author:  Middleware
  Email:   middleware@vt.edu
  Version: $Revision$
  Updated: $Date$
*/
package edu.vt.middleware.cas.authentication.support;

import java.net.URI;
import java.net.URL;
import java.security.cert.X509CRL;
import java.security.cert.X509Certificate;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import net.sf.ehcache.Cache;
import net.sf.ehcache.Element;
import org.bouncycastle.asn1.ASN1Sequence;
import org.bouncycastle.asn1.DERIA5String;
import org.bouncycastle.asn1.x509.DistributionPoint;
import org.bouncycastle.asn1.x509.DistributionPointName;
import org.bouncycastle.asn1.x509.GeneralName;
import org.cryptacular.x509.ExtensionReader;
import org.jasig.cas.adaptors.x509.authentication.handler.support.AbstractCRLRevocationChecker;
import org.jasig.cas.adaptors.x509.util.CertUtils;
import org.springframework.core.io.UrlResource;

/**
 * Cryptacular-based CRL distribution point revocation checker.
 *
 * @author Marvin S. Addison
 * @version  $Revision$
 */
public class CRLDistributionPointRevocationChecker extends AbstractCRLRevocationChecker {

    /** CRL cache. */
    private final Cache crlCache;


    /**
     * Creates a new instance that uses the given cache instance for CRL caching.
     *
     * @param crlCache Cache for CRL data.
     */
    public CRLDistributionPointRevocationChecker(final Cache crlCache) {
        if (crlCache == null) {
            throw new IllegalArgumentException("Cache cannot be null.");
        }
        this.crlCache = crlCache;
    }

    /**
     * {@inheritDoc}
     * @see AbstractCRLRevocationChecker#getCRL(X509Certificate)
     */
    @Override
    protected X509CRL getCRL(final X509Certificate cert) {
        final URL[] urls = getDistributionPoints(cert);
        log.debug(String.format(
            "Distribution points for %s: %s.",
            CertUtils.toString(cert), Arrays.asList(urls)));

        Element item;
        for (URL url : urls) {
            item = this.crlCache.get(url);
            if (item != null) {
                log.debug("Found CRL in cache for " + CertUtils.toString(cert));
                return (X509CRL) item.getObjectValue();
            }
        }

        // Try all distribution points and stop at first fetch that succeeds
        X509CRL crl = null;
        for (int i = 0; i < urls.length && crl == null; i++) {
            log.info("Attempting to fetch CRL at " + urls[i]);
            try {
                crl = CertUtils.fetchCRL(new UrlResource(urls[i]));
                log.info("Success. Caching fetched CRL.");
                this.crlCache.put(new Element(urls[i], crl));
            } catch (final Exception e) {
                log.error("Error fetching CRL at " + urls[i], e);
            }
        }

        return crl;
    }

    /**
     * Gets the CRL distrubtion points mentioned on the certificate.
     *
     * @param cert Certificate.
     *
     * @return Array of URLs mentioned in CRL distribution points on cert.
     */
    private URL[] getDistributionPoints(final X509Certificate cert) {
        final List<DistributionPoint> points;
        try {
            points = new ExtensionReader(cert).readCRLDistributionPoints();
        } catch (final Exception e) {
            log.error(
                "Error reading CRLDistributionPoints extension field on " + CertUtils.toString(cert), e);
            return new URL[0];
        }

        final List<URL> urls = new ArrayList<URL>();
        for (DistributionPoint point : points) {
            final DistributionPointName pointName = point.getDistributionPoint();
            final ASN1Sequence nameSequence = ASN1Sequence.getInstance(pointName.getName());
            for (int i = 0; i < nameSequence.size(); i++) {
                final GeneralName name = GeneralName.getInstance(nameSequence.getObjectAt(i));
                log.debug("Found CRL distribution point " + name);
                try {
                    addURL(urls, DERIA5String.getInstance(name.getName()).getString());
                } catch (Exception e) {
                    log.warn("Unsupported CRL distribution point name " + name);
                }
            }
        }

        return urls.toArray(new URL[urls.size()]);
    }

    /**
     * Parses a string URL representation and adds it to the given list.
     *
     * @param list List to which to add parsed URI.
     * @param uriString URI to parse and add to list.
     */
    private void addURL(final List<URL> list, final String uriString) {
        try {
            // Build URI by components to facilitate proper encoding of querystring
            // e.g. http://example.com:8085/ca?action=crl&issuer=CN=CAS Test User CA
            final URL url = new URL(uriString);
            final URI uri = new URI(url.getProtocol(), url.getAuthority(), url.getPath(), url.getQuery(), null);
            list.add(uri.toURL());
        } catch (final Exception e) {
            log.warn(uriString + " is not a valid distribution point URI.");
        }
    }
}
