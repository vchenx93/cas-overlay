/*
  $Id$

  Copyright (C) 2009 Virginia Tech.
  All rights reserved.

  SEE LICENSE FOR MORE INFORMATION

  Author:  Middleware
  Email:   middleware@vt.edu
  Version: $Revision$
  Updated: $Date$
*/
package edu.vt.middleware.cas.authentication.principal;

import java.security.cert.X509Certificate;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import edu.vt.middleware.cas.util.X509Helper;
import org.jasig.cas.adaptors.x509.authentication.principal
    .X509CertificateCredentials;
import org.jasig.cas.authentication.principal.Credentials;

/**
 * Resolves a principal with attributes given a X.509 certificate credential,
 * e.g. a Personal Digital Certificate in VT parlance.
 *
 * @author Middleware
 * @version $Revision$
 *
 */
public class PDCCredentialsToPrincipalResolver extends AbstractCredentialsToPrincipalResolver {

    /** Pattern used to extract UID from certificate subject */
    private static final Pattern UID_PATTERN =
        Pattern.compile(".*UID=(\\d+).*", Pattern.CASE_INSENSITIVE);


    /** {@inheritDoc} */
    public boolean supports(final Credentials credentials) {
        return credentials instanceof X509CertificateCredentials;
    }


    /** {@inheritDoc} */
    public String resolvePrincipalId(final Credentials credentials) {

        final X509CertificateCredentials certCredentials = (X509CertificateCredentials) credentials;
        X509Certificate userCert = certCredentials.getCertificate();
        if (userCert == null) {
            userCert = X509Helper.getUserCertificate(certCredentials.getCertificates());
        }
        if (userCert == null) {
            logger.warn("Certificate not found in X509CertificateCredentials.");
            return null;
        }

        final String uid = getUidFromCert(userCert);
        if (uid == null) {
            logger.warn("UID attribute not found in certificate DN.");
        }
        return uid;
    }


    /**
     * Obtains the value of the UID part of the certificate DN.
     * @param cert X.509 certificate.
     * @return The ED UID attribute value on the DN.
     */
    private String getUidFromCert(final X509Certificate cert) {
        final Matcher matcher = UID_PATTERN.matcher(
                cert.getSubjectDN().getName());
        if (matcher.matches()) {
            return matcher.group(1);
        } else {
            return null;
        }
    }

}
