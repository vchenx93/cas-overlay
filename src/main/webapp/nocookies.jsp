<jsp:directive.include file="/WEB-INF/view/jsp/includes/error-top.jsp" />

<h1>Session Cookies Required</h1>
<div id="error">
  <p class="error">It appears you have session cookies disabled in your browser. You MUST enable session cookies in
  order to use CAS, which requires them to maintain state on the login form.</p>
</div>

<jsp:directive.include file="/WEB-INF/view/jsp/includes/error-bottom.jsp" />
