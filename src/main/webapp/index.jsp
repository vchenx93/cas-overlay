<%@ page session="true" %>
<%@ taglib prefix="c" uri="/WEB-INF/tld/c.tld" %>
<c:choose>
  <c:when test="${not empty pageContext.request.queryString}">
    <c:redirect url="/login?${pageContext.request.queryString}" />
  </c:when>
  <c:otherwise>
    <c:redirect url="/login" />
  </c:otherwise>
</c:choose>
