<jsp:directive.include file="includes/error-top.jsp" />

<div id="title">404 - Not Found</div>
<div id="error">
  <p class="error">The page you have requested does not exist.</p>
</div>

<jsp:directive.include file="includes/error-bottom.jsp" />
