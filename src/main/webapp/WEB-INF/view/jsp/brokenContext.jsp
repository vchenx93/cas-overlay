<jsp:directive.include file="includes/error-top.jsp" />

<div id="title">CAS Unavailable</div>
<div id="error">
  <p class="error">The CAS service encountered an error on startup and is not
  available.</p>
</div>
<p>If this error persists, please submit a problem report to
<a href="http://4help.vt.edu">VT 4Help</a>.  Please include the following
server name in your 4Help problem report:
<strong>${pageContext.request.serverName}</strong>.</p>

<jsp:directive.include file="includes/error-bottom.jsp" />
