<jsp:directive.include file="includes/error-top.jsp" />

<div id="title">500 - Server Error</div>
<div id="error">
<p class="error">CAS encountered an unexpected error:</p>
<%
  // unwrap ServletExceptions
  // isErrorPage directive creates implicit "exception" variable that holds
  // untrapped exception that bubbles up to container
  if (exception != null) {
    while (exception instanceof javax.servlet.ServletException) {
      exception = ((javax.servlet.ServletException) exception)
          .getRootCause();
    }
    request.setAttribute("error", exception);
    request.setAttribute("errorClass", exception.getClass().getSimpleName());
  }
  request.setAttribute("address", java.net.InetAddress.getLocalHost());
%>
<p><strong>${requestScope.errorClass}</strong>: <c:out value="${requestScope.error.message}" /></p>
</div>
<p>If this error persists, please submit a problem report to <a
	href="http://4help.vt.edu">VT 4Help</a>. Please include the following
server name in your 4Help problem report:
<strong>${requestScope.address.hostName}</strong>.</p>

<jsp:directive.include file="includes/error-bottom.jsp" />
