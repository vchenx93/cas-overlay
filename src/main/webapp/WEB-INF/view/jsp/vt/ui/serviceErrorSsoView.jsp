<jsp:directive.include file="includes/top.jsp" />

<div id="content">
  <div id="title"><spring:message code="screen.service.sso.error.title" /></div>

  <div id="error">
    <p class="error"><spring:message code="screen.service.sso.error.message" /></p>
    <p>
      <spring:message code="screen.service.sso.error.message2"
        arguments="${fn:escapeXml(request.requestURI)}" />
    </p>
  </div>
</div>

<jsp:directive.include file="includes/bottom.jsp" />
