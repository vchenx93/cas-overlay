<jsp:directive.include file="includes/top.jsp" />

<vt:casUrl value="/login" var="loginUrl" />
<c:url value="/iforgot?type=password" var="iforgotUrl" />
<div id="content">
  <div id="title">Forgotten Credential Help</div>

  <div id="info">
    <p>Your username is the <strong>email address</strong> where your Virginia
    Tech guest invitation was delivered.  Search your email accounts for a
    message with the following text:<p>
    
    <blockquote>You have been invited to create a Virginia Tech guest account
    using your email address.</blockquote>

    <p>The address of the email account containing that message is your username.</p>
    <div class="big-buttons">
      <a class="button" href="${loginUrl}">Go to Login</a>
      <a class="button" href="${iforgotUrl}">Forgot Password</a>
    </div>
  </div>
</div>

<jsp:directive.include file="includes/bottom.jsp" />
