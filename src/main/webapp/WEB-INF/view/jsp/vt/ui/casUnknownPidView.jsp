<jsp:directive.include file="includes/top.jsp" />

<div id="content">
  <div id="title">Forgotten Credential Help</div>
  <div id="info">
    <p>Please call Virginia Tech 4Help for further assistance:</p>
    <p><strong><a href="tel:540-231-4357">540-231-4357</a></strong></p>
  </div>
</div>

<jsp:directive.include file="includes/bottom.jsp" />
