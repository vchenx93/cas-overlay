<%@ page session="false" %>
<%@ page pageEncoding="UTF-8" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%@ page import="org.springframework.util.StringUtils"%>
<%@ taglib prefix="c" uri="/WEB-INF/tld/c.tld" %>
<%@ taglib prefix="fmt" uri="/WEB-INF/tld/fmt.tld" %>
<%@ taglib prefix="fn" uri="/WEB-INF/tld/fn.tld" %>
<%@ taglib prefix="spring" uri="/WEB-INF/tld/spring.tld" %>
<%@ taglib prefix="form" uri="/WEB-INF/tld/spring-form.tld" %>
<%@ taglib prefix="vt" uri="/WEB-INF/tld/vt.tld" %>
<spring:theme code="mobile.css" var="mobileCss" text="" />
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
        "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>Virginia Tech Central Authentication Service</title>
<c:choose>
  <c:when test="${not empty requestScope['isMobile'] and not empty mobileCss}">
    <meta name="viewport"
      content="width=device-width; initial-scale=1.0; maximum-scale=1.0; user-scalable=0;" />
    <meta name="apple-mobile-web-app-capable" content="yes" />
    <meta name="apple-mobile-web-app-status-bar-style" content="black" />
    <link type="text/css" rel="stylesheet" href="<c:url value="${mobileCss}" />" />
    <spring:theme code="mobile.header.image" var="headerImage" />
    <c:set var="themeName" value="mobile" />
  </c:when>
  <c:otherwise>
    <c:set var="themeName" value="standard" />
    <spring:theme code="standard.css" var="standardCss" />
    <spring:theme code="standard.header.image" var="headerImage" />
    <link type="text/css" rel="stylesheet" href="<c:url value="${standardCss}" />" />
  </c:otherwise>
</c:choose>
<link rel="shortcut icon" href="<c:url value="/favicon.png"/>" type="image/png" />
<script type="text/javascript" src="//ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script>
<script type="text/javascript" src="<c:url value="/scripts/cas.js" />"></script>
</head>

<body id="cas">
<div id="page">

<div id="header">
  <c:if test="${not empty requestScope['isMobile']}">
    <a id="help" href="<c:url value="/help#login"/>">Help</a>
  </c:if>
  <img src="<c:url value="${headerImage}"/>"
    alt="Virginia Tech Central Authentication Service" />
</div>

<c:if test="${empty requestScope['isMobile']}">
  <c:url value="/help" var="helpUrl" />
  <div id="links" class="fade-blue">
    <div id="help" class="link"><a target="_help" href="${helpUrl}#login" onclick="popup('${helpUrl}#login', '_help')">Help</a></div>
    <div class="link"><a target="_help" href="${helpUrl}#terms" onclick="popup('${helpUrl}#terms', '_help')">Terms of Use</a></div>
    <div class="link"><a target="_help" href="${helpUrl}#about" onclick="popup('${helpUrl}#about', '_help')">About CAS</a></div>
    <div class="clear"></div>
  </div>  
</c:if>
