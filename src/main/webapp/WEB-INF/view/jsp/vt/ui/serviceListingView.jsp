<%@include file="includes/top.jsp"%>

<div id="content">
<%@include file="includes/top-secure.jsp"%>
<div id="title"><spring:message code="management.services.listing.title" text="Service Management" /></div>

<c:if test="${fn:length(services) eq 0}">
  <c:url value="/" var="serviceManagerUrl" />
  <div id="error" class="error">
    <spring:message code="management.services.service.warn" arguments="${serviceManagerUrl}" />
  </div>
</c:if>

<c:if test="${not empty status}">
  <div id="info">
    <spring:message code="management.services.status.${status}" arguments="${serviceName}" />
  </div>
</c:if>

<div id="tableWrapper" class="tableWrapper">
  <table cellspacing="0">
    <thead>
    <tr>
    <th>Order</th>
	  <th><spring:message code="management.services.manage.label.name" /></th>
	  <th><spring:message code="management.services.manage.label.serviceUrl" /></th>
	  <th><spring:message code="management.services.manage.label.enabled" /></th>
	  <th><spring:message code="management.services.manage.label.allowedToProxy" /></th>
	  <th><spring:message code="management.services.manage.label.ssoParticipant" /></th>
	  <th>&nbsp;</th>
	  <th>&nbsp;</th>
    </tr>
    </thead>
    
    <tbody>
  <c:url value="/images/services"  var="serviceImageUrl" />
  <c:forEach items="${services}" var="service" varStatus="status">
  <tr id="row${status.index}"${param.id eq service.id ? ' class="added"' : ''}>
    <td>${service.evaluationOrder}</td>
    <td id="${service.id}" class="td1">${service.name}</td>
    <td class="td2">${fn:length(service.serviceId) < 50 ? service.serviceId : fn:substring(service.serviceId, 0, 50)}</td>
    <td class="ac td3"><img src="${serviceImageUrl}/${service.enabled}.gif" alt="${service.enabled ? 'Enabled' : 'Disabled'}" /></td>
    <td class="ac td4"><img src="${serviceImageUrl}/${service.allowedToProxy}.gif" alt="${service.allowedToProxy ? 'Allowed to Proxy' : 'Not Allowed to Proxy'}" /></td>
    <td class="ac td5"><img src="${serviceImageUrl}/${service.ssoEnabled}.gif" alt="${service.ssoEnabled ? 'SSO Enabled' : 'SSO Disabled'}" /></td>

    <td class="td6" id="edit${status.index}"><a href="edit?id=${service.id}" class="edit"><spring:message code="management.services.manage.action.edit" /></a></td>
    <td class="td7" id="delete${status.index}"><a href="delete?id=${service.id}" class="del"><spring:message code="management.services.manage.action.delete" /></a></td>
  </tr>
  </c:forEach>
    </tbody>
  </table>
</div>

<div class="buttons">
  <a class="button" href="add"><spring:message code="addServiceView" /></a>
  <a class="button" href="duplicate"><spring:message code="duplicateServiceView" /></a>
</div>

</div>

<div id="dialog-confirm" class="dialog" title="Confirm Service Deletion">
  <p class="dialog-message">Are you sure you want to delete the following service?</p>
  <p class="dialog-data"></p>
</div>

<script type="text/javascript" src="//ajax.googleapis.com/ajax/libs/jqueryui/1.8.23/jquery-ui.min.js"></script>
<script type="text/javascript">
  var loc;

  $(document).ready(function () {
    $('#dialog-confirm').dialog({
      autoOpen: false,
      disabled: true,
      resizable: false,
      height: 160,
      width: 320,
      modal: true,
      closeText: 'X',
      buttons: {
        'Yes': function() {
          $(this).dialog('close');
          window.location = loc;
        },
        'No': function() {
          $(this).dialog('close');
        }
      }
    });

    $('a.del').click(function(event) {
      loc = $(this).attr('href');
      $('#dialog-confirm p.dialog-data').text($(this).parents('tr').children('td.td1').text());
      $('#dialog-confirm').dialog('open');
      event.preventDefault();
    });
  });
</script>

<%@include file="includes/bottom.jsp" %>