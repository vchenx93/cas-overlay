<%@include file="includes/top.jsp"%>

<div id="content">
  <div id="title">Logged Out of Secure Area</div>
  <div id="info">
    <p>You have successfully logged out of the secure area, but are still logged into CAS.
      Follow the link below to log out of CAS, which will end the sessions of all services accessed via CAS.</p>
    <div class="big-buttons">
      <a class="button" href="<c:url value="/logout" />">Log Out of CAS</a>
    </div>
   </div>

</div>

<%@include file="includes/bottom.jsp" %>