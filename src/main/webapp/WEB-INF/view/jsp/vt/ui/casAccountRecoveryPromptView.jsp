<jsp:directive.include file="includes/top.jsp" />

<div id="content">
<div id="title"><spring:message code="screen.account.recovery.title" /></div>

<div id="warn">
  <h2><spring:message code="screen.account.recovery.h2" /></h2>
  <p>
    <c:choose>
      <c:when test="${not empty accountRecoveryMaintenanceDate}">
        <spring:message code="screen.account.recovery.verifyText" arguments="${maxDays}" />
        <spring:message code="screen.account.recovery.verifyLinkText" var="manageText"/>
      </c:when>
      <c:otherwise>
        <spring:message code="screen.account.recovery.setText"/>
        <spring:message code="screen.account.recovery.setLinkText" var="manageText"/>
      </c:otherwise>
    </c:choose>
    <spring:message code="screen.account.recovery.nagText"/>
  </p>
  <div class="spacer"></div>
  
  <vt:casUrl value="/login" var="bypassUrl" />
  <div class="big-buttons">
    <a class="button" href="${accountRecoveryManagementUrl}">${manageText}</a>
    <a class="button" href="${bypassUrl}">
      <spring:message code="screen.account.recovery.bypassLinkText" />
    </a>
  </div>
</div>

</div>

<jsp:directive.include file="includes/bottom.jsp" />