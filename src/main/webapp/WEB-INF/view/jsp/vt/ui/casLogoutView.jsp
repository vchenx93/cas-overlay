<jsp:directive.include file="includes/top.jsp" />

<div id="content">
  <div id="title"><spring:message code="screen.logout.title" /></div> 

  <div id="info">
    <p class="info"><spring:message code="screen.logout.success" /></p>
    <c:if test="${not empty param['url']}">
      <p><spring:message code="screen.logout.redirect"
        arguments="${fn:escapeXml(param.url)}" /></p>
    </c:if>
  </div>
  <p><spring:message code="screen.logout.security" /></p>
</div>

<jsp:directive.include file="includes/bottom.jsp" />
