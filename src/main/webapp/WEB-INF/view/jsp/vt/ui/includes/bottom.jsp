</div> <!-- END PAGE -->
<div id="footer">
<p>&copy; 2008-2014 Virginia Polytechnic Institute and State University</p>
<p>The VT CAS logo is a derivative of <a href="http://www.flickr.com/photos/brassman/122236326/">Night Safe</a>
    by brassman, licensed under <a href="http://creativecommons.org/licenses/by-nc-sa/2.0/legalcode">BY-NC-SA</a>.
</p>
</div>

</body>
</html>