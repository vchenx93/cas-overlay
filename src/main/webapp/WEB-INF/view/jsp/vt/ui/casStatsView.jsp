<jsp:directive.include file="includes/top.jsp" />

<div id="content">
  <%@include file="includes/top-secure.jsp"%>
  <div id="title">CAS Statistics</div>

  <fmt:formatNumber var="freeWidth" type="number" maxFractionDigits="0"
                    value="${600 * freeMemory / (freeMemory + usedMemory)}" />
  <fmt:formatNumber var="usedWidth" type="number" maxFractionDigits="0"
                    value="${600 * usedMemory/(freeMemory + usedMemory)}" />

  <div>
    <div><strong>Host Name:</strong> ${hostname}</div>
    <div><strong>Start Time:</strong> ${startTime}</div>
    <div><strong>Uptime:</strong> ${upTime}</div>
    <div><strong>Memory Usage:</strong>
      <div style="margin-top:0.2em;border:1px solid #999;width:620px">
        <div style="float:left;padding:5px;color:black;background-color:#3f3;width:${freeWidth}px">Free:&nbsp;${freeMemory}M</div>
        <div style="float:left;padding:5px;color:white;background-color:#a00;width:${usedWidth}px">Used:&nbsp;${usedMemory}M</div>
        <div style="clear:both"></div>
      </div>
    </div>
  </div>
</div>

<jsp:directive.include file="includes/bottom.jsp" />
