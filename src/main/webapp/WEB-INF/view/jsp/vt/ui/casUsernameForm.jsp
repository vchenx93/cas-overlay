<jsp:directive.include file="includes/top.jsp" />

<div id="content">

<div id="title">Forgotten Credential Help</div>

<form:form method="get" id="affiliation_form" htmlEscape="true" cssClass="short-form">
  <fieldset class="short">
    <h2>What is your username?</h2>
    <jsp:directive.include file="includes/show-global-errors.jsp" />
    <div class="field">
      <label for="username"><spring:message code="screen.login.label.username" /></label>
      <input type="text" name="username" class="required" id="username"
        value="${flowScope.username}"
        tabindex="1" accesskey="u" size="14"/>
    </div>
    <div class="buttons">
      <input type="hidden" name="execution" value="${flowExecutionKey}" />
      <button name="_eventId_next" accesskey="n" value="next" tabindex="2" type="submit">Next</button>
      <button name="_eventId_back" accesskey="b" value="back" tabindex="3" type="submit">Back</button>
    </div>
  </fieldset>
</form:form>

</div>

<jsp:directive.include file="includes/bottom.jsp" />
