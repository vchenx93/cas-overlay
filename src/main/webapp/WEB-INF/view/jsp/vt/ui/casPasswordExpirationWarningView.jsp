<jsp:directive.include file="includes/top.jsp" />

<div id="content">
<div id="title"><spring:message code="screen.password.warn.title" /></div>

<div id="warn">
  <h2><spring:message code="screen.password.warn.h2" /></h2>
  <p>
    <spring:message code="screen.password.warn.nagText" arguments="${ttl}"/>
  </p>
  <div class="spacer"></div>
  
  <vt:casUrl value="/login" var="bypassUrl" />
  <div class="big-buttons">
    <a class="button" href="${passwordChangeUrl}">
      <spring:message code="screen.password.warn.changeLinkText"/>
    </a>
    <a class="button" href="${bypassUrl}">
      <spring:message code="screen.password.warn.bypassLinkText" />
    </a>
  </div>
</div>

</div>

<jsp:directive.include file="includes/bottom.jsp" />