<jsp:directive.include file="includes/top.jsp" />

<div id="content">

<div id="title">Forgotten Credential Help</div>

<form:form method="get" id="affiliation_form" htmlEscape="true" cssClass="short-form">
  <fieldset class="short">
    <h2>What is your affiliation?</h2>
    <jsp:directive.include file="includes/show-global-errors.jsp" />
    <div class="field checkboxes">
      <input id="affil_vt" name="affiliation" value="vt" tabindex="1"
        accesskey="v" type="radio" />
      <label for="affil_vt">Student, faculty, staff, or other university affiliate</label>
    </div>
    <div class="field checkboxes">
      <input id="affil_guest" name="affiliation" value="guest" tabindex="2"
        accesskey="g" type="radio" />
      <label for="affil_guest">Something else (guest)</label>
    </div>
    <script language="javascript" type="text/javascript">
      $('.checkboxes input').click({form:$('#affiliation_form'), eventId:'next'}, swf_submit_handler);
    </script>
    <div class="buttons">
      <input type="hidden" name="execution" value="${flowExecutionKey}" />
      <button name="_eventId_next" accesskey="n" value="next" tabindex="3" type="submit">Next</button>
      <button name="_eventId_back" accesskey="b" value="back" tabindex="4" type="submit">Back</button>
    </div>
  </fieldset>
</form:form>

</div>

<jsp:directive.include file="includes/bottom.jsp" />
