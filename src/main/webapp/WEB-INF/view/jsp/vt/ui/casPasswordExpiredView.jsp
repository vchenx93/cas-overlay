<jsp:directive.include file="includes/top.jsp" />

<div id="content">
<div id="title"><spring:message code="screen.password.exp.title" /></div>

<div id="warn">
  <h2><spring:message code="screen.password.exp.h2" /></h2>
  <p><spring:message code="screen.password.exp.notice" /></p>
  <div class="spacer"></div>
  <div class="big-buttons">
    <a class="button" href="${passwordChangeUrl}">
      <spring:message code="screen.password.exp.linkText"/>
    </a>
  </div>
</div>

</div>

<jsp:directive.include file="includes/bottom.jsp" />