<jsp:directive.include file="includes/top.jsp" />

<div id="content">
<%-- Only display service information for services with a description --%>
<c:if test="${not empty registeredService and not empty registeredService.description}">
  <div>
    <h2>Login to ${registeredService.name}</h2>
    <p><c:out value="${registeredService.description}" escapeXml="false" /></p>
  </div>
</c:if>
<spring:hasBindErrors name="${commandName}">
  <c:if test="${errors.errorCount > 0}">
    <div id="login-error">
    <c:forEach items="${errors.allErrors}" var="e">
      <span class="error">${e.defaultMessage}</span>
    </c:forEach>
    </div>
  </c:if>
</spring:hasBindErrors>

<vt:casUrl value="/iforgot" var="iforgotUrl" />

<form:form method="post" id="login-form" commandName="${commandName}"
  htmlEscape="true">
  <fieldset class="short" style="margin-top:20px">
    <div class="field">
      <label for="username">
        <spring:message code="screen.login.label.username" />
      </label>
      <c:if test="${not empty flowScope.openIdLocalId}">
        <strong>${flowScope.openIdLocalId}</strong>
        <input type="hidden" id="username" name="username"
         value="${flowScope.openIdLocalId}" />
      </c:if>
      <c:if test="${empty flowScope.openIdLocalId}">
        <spring:message code="screen.login.label.username.accesskey"
         var="userNameAccessKey" />
        <form:input cssClass="required hintable" id="username"
         tabindex="1" accesskey="${userNameAccessKey}" path="username"
         size="14" autocomplete="false" htmlEscape="true" />
      </c:if>
      <div class="field-hint">PID or Guest ID</div>
    </div>

    <div class="field">
      <label for="password">
        <spring:message code="screen.login.label.password" />
      </label>
      <%--
      NOTE: Certain browsers offer the option of caching passwords for a user.
      There is a non-standard attribute, "autocomplete" that when set to "off"
      will tell certain browsers not to prompt to cache credentials.  For more
      information, see the following web page:
      http://www.geocities.com/technofundo/tech/web/ie_autocomplete.html
      --%>
      <spring:message code="screen.login.label.password.accesskey"
       var="passwordAccessKey" />
      <form:password cssClass="required" id="password"
       tabindex="2" path="password" accesskey="${passwordAccessKey}"
       size="14" htmlEscape="true" />
      <div class="note"><span class="smaller">
        <a class="nav" href="${iforgotUrl}">Forgot username or password?</a></span>
      </div>
    </div>
    <div class="field checkboxes">
      <input id="optwarn" name="warn" value="true" tabindex="3"
        accesskey="<spring:message code="screen.login.label.warn.accesskey" />"
        type="checkbox" /><label for="optwarn">
        <spring:message code="screen.login.label.warn" />
      </label>
    </div>
    <div class="buttons">
      <input type="hidden" name="lt" value="${loginTicket}" />
      <input type="hidden" name="execution" value="${flowExecutionKey}" />
      <input type="hidden" name="_eventId" value="submit" />
      <button name="submit" accesskey="l" value="_submit" tabindex="4" type="submit">
        <spring:message code="screen.login.button.login" />
      </button>
      <button name="reset" accesskey="c" value="_reset" tabindex="5" type="reset">
        <spring:message code="screen.login.button.clear" />
      </button>
    </div>
  </fieldset>
</form:form>

<vt:casUrl value="/login?pdc=1" var="pdcLoginUrl" />
<c:if test="${empty requestScope['isMobile']}">
  <p id="switch">
    <spring:message code="screen.login.switch" arguments="${pdcLoginUrl}" />
  </p>
</c:if>

<h2><spring:message code="screen.login.security.header" /></h2>
<p><spring:message code="screen.login.security.message" /></p>
</div> <!-- END CONTENT -->

<jsp:directive.include file="includes/bottom.jsp" />
