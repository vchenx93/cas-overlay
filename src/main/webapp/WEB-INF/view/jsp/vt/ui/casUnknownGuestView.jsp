<jsp:directive.include file="includes/top.jsp" />

<div id="content">
  <div id="title">Forgotten Credential Help</div>
  <div id="info">
    <p>The guest user <strong>${username}</strong> cannot be found in the
    Virginia Tech directory.  We recommend you try the guided username help
    feature.</p>
    <div class="big-buttons">
      <a class="button" href="<c:url value="/iforgot?type=username"/>">Forgot Username</a>
    </div>
  </div>
</div>

<jsp:directive.include file="includes/bottom.jsp" />
