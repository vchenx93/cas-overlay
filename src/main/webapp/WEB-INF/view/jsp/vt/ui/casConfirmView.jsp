<jsp:directive.include file="includes/top.jsp" />

<div id="content">
  <div id="title"><spring:message code="screen.confirmation.title" /></div>
  <div id="warn">
    <c:url var="serviceUrl" value="${fn:escapeXml(requestScope.service.id)}" />
    <c:url var="continueUrl" value="${serviceUrl}">
      <c:param name="ticket" value="${serviceTicketId}" />
    </c:url>
    <spring:message code="screen.confirmation.abortUrl" var="abortUrl" />
    <p>
      <spring:message code="screen.confirmation.message"/>
    </p>
    <div class="bigger"><strong>${serviceUrl}</strong></div>
    <div class="big-buttons">
      <a class="button" href="${continueUrl}">
        <spring:message code="screen.confirmation.continue" />
      </a>
      <a class="button" href="${abortUrl}">
        <spring:message code="screen.confirmation.abort" />
      </a>
    </div>
  </div>
</div>

<jsp:directive.include file="includes/bottom.jsp" />
