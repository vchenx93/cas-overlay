<jsp:directive.include file="includes/top.jsp" />


<c:url value="/login" var="loginUrl">
  <c:if test="${not empty param.pdc}">
    <c:param name="pdc" value="1" />
  </c:if>
</c:url>

<vt:casUrl value="${loginUrl}" var="constructedUrl" />

<div id="content">
  <div id="title"><spring:message code="screen.login.timeout.title" /></div> 
  <div id="warn">
    <p><spring:message code="screen.login.timeout.message" /></p>
    <spring:message code="screen.login.timeout.back" var="linkText" />
    <div class="big-buttons">
      <a class="button" href="${loginUrl}">${linkText}</a>
    </div>
  </div>
</div>

<jsp:directive.include file="includes/bottom.jsp" />
