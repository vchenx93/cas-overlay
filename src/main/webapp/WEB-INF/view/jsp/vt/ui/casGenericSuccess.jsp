<jsp:directive.include file="includes/top.jsp" />

<div id="content">
  <div id="title"><spring:message code="screen.success.title" /></div>

  <div id="info">
    <p class="info"><spring:message code="screen.success.success" /></p>
  </div>
  <p><spring:message code="screen.success.security" /></p>
</div>

<jsp:directive.include file="includes/bottom.jsp" />
