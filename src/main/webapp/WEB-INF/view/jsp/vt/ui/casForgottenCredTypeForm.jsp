<jsp:directive.include file="includes/top.jsp" />

<div id="content">

<div id="title">Forgotten Credential Help</div>

<form:form method="get" id="type_form" htmlEscape="true" cssClass="short-form">
  <fieldset class="short">
    <h2>What did you forget?</h2>
    <jsp:directive.include file="includes/show-global-errors.jsp" />
    <div class="field checkboxes">
      <input id="type_username" name="type" value="username" tabindex="1"
        accesskey="u" type="radio" />
      <label for="type_username">
        I forgot my <strong>username</strong></label>
    </div>
    <div class="field checkboxes">
      <input id="type_password" name="type" value="password" tabindex="2"
        accesskey="p" type="radio" />
      <label for="type_password">
        I forgot my <strong>password</strong></label>
    </div>
    <script language="javascript" type="text/javascript">
      $('.checkboxes input').click({form:$('#type_form'), eventId:'next'}, swf_submit_handler);
    </script>
    <div class="buttons">
      <input type="hidden" name="execution" value="${flowExecutionKey}" />
      <button name="_eventId_next" accesskey="n" value="next" tabindex="3" type="submit">Next</button>
    </div>
  </fieldset>
</form:form>

</div>

<jsp:directive.include file="includes/bottom.jsp" />
