<%@include file="includes/top.jsp"%>

<div id="content">
<%@include file="includes/top-secure.jsp"%>
<div id="title"><spring:message code="management.services.${action}.title" arguments="${service.name}" /></div>

<form:form action="save" commandName="service">
  <c:if test="${not empty successMessage}">
    <div id="info">${successMessage}</div>
  </c:if>
  
  <p class="instructions">
    <spring:message code="management.services.add.instructions" />
  </p>

  <fieldset>
    <div class="field">
      <form:errors path="name" cssClass="error" element="div" />
      <label for="name">
        <spring:message code="management.services.add.property.name" />
      </label>
      <form:input path="name" cssClass="required" />
    </div>
    <div class="field">
      <form:errors path="serviceId" cssClass="error" element="div" />
      <label for="serviceId">
        <spring:message code="management.services.add.property.serviceUrl" />
      </label>
      <form:input path="serviceId" cssClass="required" />
    </div>
    <div class="field">
      <form:errors path="description" cssClass="error" element="div" />
      <label for="description">
        <spring:message code="management.services.add.property.description" />
      </label>
      <form:textarea path="description" cssClass="required" />
    </div>
    <div class="field">
      <form:errors path="theme" cssClass="error" element="div" />
      <label for="theme">
        <spring:message code="management.services.add.property.themeName" />
      </label>
      <form:input path="theme" cssClass="required" />
    </div>
    <div class="field checkboxes">
      <form:checkbox path="enabled" value="true" />
      <label for="enabled1" id="enabled-l">
        <spring:message code="management.services.add.property.status.enabled" />
      </label>
    </div>
    <div class="field checkboxes">
      <form:checkbox path="allowedToProxy" value="true" />
      <label for="allowedToProxy1" id="proxy-l">
        <spring:message code="management.services.add.property.status.allowedToProxy" />
      </label>
    </div>
    <div class="field checkboxes">
      <form:checkbox path="ssoEnabled" value="true" />
      <label for="ssoEnabled1" id="ssl-l" class="checkbox">
        <spring:message code="management.services.add.property.status.ssoParticipant" />
      </label>
    </div>
    <div class="field checkboxes">
      <form:checkbox path="anonymousAccess" value="true" />
      <label for="anonymousAccess1" id="anonymousAccess-l">
        <spring:message code="management.services.add.property.status.anonymousAccess" />
      </label>
    </div>
    <div class="field">
      <label for="allowedAttributes">
        <spring:message code="management.services.add.property.attributes" />
      </label>
      <form:select path="allowedAttributes" items="${availableAttributes}" multiple="true" />
    </div>
    <div class="field checkboxes">
      <form:checkbox path="ignoreAttributes" value="true" />
      <label for="ignoreAttributes1" id="ignoreAttributes-l">
        <spring:message code="management.services.add.property.ignoreAttributes" />
      </label>
    </div>
    <div class="field">
      <form:errors path="evaluationOrder" cssClass="error" element="div" />
      <label for="theme">
        <spring:message code="management.services.add.property.evaluationOrder" />
      </label>
      <form:input path="evaluationOrder" cssClass="required number-field" />
    </div>
    <div class="buttons">
      <button type="submit" class="primaryAction" id="submit" value="Save Changes">
        <spring:message code="management.services.add.button.save" />
      </button>
      <a class="button" href="manage">
        <spring:message code="management.services.add.button.cancel" />
      </a>
    </div>
  </fieldset>
</form:form>

</div>
<%@include file="includes/bottom.jsp" %>