<c:forEach items="${flowRequestContext.messageContext.allMessages}" var="message">
  <c:if test="${message.severity eq 'ERROR' && empty message.source}">
    <c:set var="hasGlobalErrors" value="true" />
  </c:if>
</c:forEach>
<c:if test="${hasGlobalErrors}"><p></c:if>
<c:forEach items="${flowRequestContext.messageContext.allMessages}" var="message">
  <c:if test="${message.severity eq 'ERROR' && empty message.source}">
    <div class="error">${message.text}</div>
  </c:if>
</c:forEach>
<c:if test="${hasGlobalErrors}"></p></c:if>
