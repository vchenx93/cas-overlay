<%@include file="includes/top.jsp"%>

<div id="content">
<%@include file="includes/top-secure.jsp"%>
<div id="title"><spring:message code="management.services.duplicate.title" text="Duplicate Service" /></div>

<form:form action="duplicate" method="POST" commandName="spec">
  <c:if test="${not empty successMessage}">
    <div id="info">${successMessage}</div>
  </c:if>

  <form:errors path="*">
    <div id="error">
      <h3 class="error">Duplication Failed</h3>
      <form:errors path="*" element="div" />
    </div>
  </form:errors>

  <p class="instructions">
    <spring:message code="management.services.duplicate.instructions" />
  </p>

  <fieldset>
    <div class="field">
      <label for="sourceId">
        <spring:message code="management.services.duplicate.property.sourceId" />
      </label>
      <form:select path="sourceId" items="${services}" itemLabel="name" itemValue="id" cssClass="required" />
    </div>
    <div class="field">
      <label for="serviceUrl">
        <spring:message code="management.services.duplicate.property.serviceUrl" />
      </label>
      <form:input path="serviceUrl" cssClass="required" />
    </div>
    <div class="buttons">
      <button type="submit" class="primaryAction" id="submit">
        <spring:message code="management.services.duplicate.button.save" />
      </button>
      <a class="button" href="manage">
        <spring:message code="management.services.duplicate.button.cancel" />
      </a>
    </div>
  </fieldset>
</form:form>

</div>
<%@include file="includes/bottom.jsp" %>