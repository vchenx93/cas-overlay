<%@include file="includes/top.jsp"%>

<div id="content">
  <%@include file="includes/top-secure.jsp"%>
  <div id="title">Secure Area</div>
  <div id="info">
    <p>Management and monitoring functions of CAS are available here.</p>
    <div class="big-buttons">
      <a class="button" href="stats/">CAS Statistics</a>
      <a class="button" href="services/">Service Manager</a>
    </div>
   </div>

</div>

<%@include file="includes/bottom.jsp" %>