<jsp:directive.include file="includes/top.jsp" />

<div id="content">
  <div id="title"><spring:message code="screen.service.error.title" /></div>
  
  <div id="error">
    <p class="error">
      <spring:message code="screen.service.error.message"/>
    </p>
    <p>${fn:escapeXml(requestScope.service.id)}</p>
  </div>
</div>

<jsp:directive.include file="includes/bottom.jsp" />
