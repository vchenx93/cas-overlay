<jsp:directive.include file="includes/top.jsp" />

<vt:casUrl value="/login" var="loginUrl" />
<c:url value="/iforgot?type=password" var="iforgotUrl" />
<div id="content">
  <div id="title">Forgotten Credential Help</div>

  <div id="info">
    <p>Your username is your VT PID, which is commonly the mailbox
    name of your VT email address.  Consider the example:
   
    <blockquote><strong>hizzy123</strong>@vt.edu</blockquote>
    
    <p>Assuming the above is not a mail alias, the PID is <strong>hizzy123</strong>.</p>
    <p>Contact <a href="https://ars.cns.vt.edu/prsubmit/prsubmit.action">4Help</a> if you need further assistance.</p>
    <div class="big-buttons">
      <a class="button" href="${loginUrl}">Go to Login</a>
      <a class="button" href="${iforgotUrl}">Forgot Password</a>
    </div>
  </div>
</div>

<jsp:directive.include file="includes/bottom.jsp" />
