<jsp:directive.include file="includes/top.jsp" />

<div id="content">
<c:if test="${param.e == 1}">
  <div id="login-error">
    <p class="error"><spring:message code="screen.pdcLogin.failure" /></p>
    <spring:message code="screen.pdcLogin.causes" />
    <p><spring:message code="screen.pdcLogin.help.pdc" /></p>
  </div>
</c:if>

<vt:casUrl
    var="postUrl"
    value="https://${pageContext.request.serverName}:9443${pageContext.servletContext.contextPath}/login?pdc=1" />
<h2><spring:message code="screen.pdcLogin.header" /></h2>
<p><spring:message code="screen.pdcLogin.instructions" /></p>

<form:form method="post" id="login-form" commandName="${commandName}" action="${postUrl}" htmlEscape="true">
  <fieldset>
    <div class="field checkboxes">
      <input id="optwarn" name="warn" value="true" tabindex="4"
        accesskey="<spring:message code="screen.login.label.warn.accesskey" />"
	    type="checkbox" /><label for="optwarn">
        <spring:message code="screen.login.label.warn" />
      </label>
    </div>
    <div class="buttons">
      <input type="hidden" name="execution" value="${flowExecutionKey}" />
      <input type="hidden" name="_eventId" value="submit" />
      <button name="submit" accesskey="l" tabindex="4" type="submit" value="_submit">
        <spring:message code="screen.login.button.login" />
      </button>
    </div>
  </fieldset>
</form:form>

<vt:casUrl var="stdLoginUrl" value="/login" />
<p id="switch">
  <spring:message code="screen.pdcLogin.switch" arguments="${stdLoginUrl}" />
</p>

<h2><spring:message code="screen.login.security.header" /></h2>
<p><spring:message code="screen.login.security.message" /></p>

<c:url value="/images/incommon-bronze.png" var="bronzeImg" />
<c:url value="/images/incommon-silver.png" var="silverImg" />
<p style="width:150px;margin:0 0 0 auto">
  <a style="text-decoration:none" target="_help" href="${helpUrl}#incommon" onclick="popup('${helpUrl}#incommon', '_help')">
    <img style="padding:16px 8px 0 0" width="64" height="64" src="${bronzeImg}" border="0"/>
    <img style="padding:16px 0 0 8px" width="64" height="64" src="${silverImg}" border="0" />
  </a>
</p>

</div> <!-- END CONTENT -->

<jsp:directive.include file="includes/bottom.jsp" />
