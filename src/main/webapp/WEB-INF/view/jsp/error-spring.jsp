<jsp:directive.include file="includes/error-top.jsp" />

<div id="title">Spring Application Error</div>
<div id="error">
<p class="error">The Spring Framework used by CAS threw an unexpected exception:</p>
<c:if test="${not empty exception}">
  <%
  // Drill down to root cause
  Throwable t = (Throwable) request.getAttribute("exception");
  request.setAttribute("errorClass", t.getClass().getSimpleName());
  while (t.getCause() != null) {
    t = t.getCause();
    request.setAttribute("rootCause", t);
    request.setAttribute("rootCauseClass", t.getClass().getSimpleName());
  }
  request.setAttribute("address", java.net.InetAddress.getLocalHost());
  %>
  <p><strong>${errorClass}</strong>: <c:out value="${exception.message}" /></p>
  <c:if test="${not empty requestScope.rootCause}">
    <p class="error">Root cause:</p>
    <p><strong>${requestScope.rootCauseClass}</strong>:
    <c:out value="${requestScope.rootCause.message}" /><br/>
    <c:forEach items="${requestScope.rootCause.stackTrace}" var="st" varStatus="stat">
      <c:if test="${stat.count <= 5}">
	      <c:out value="${st}" /><br/>
	    </c:if>
    </c:forEach>
    <c:if test="${fn:length(requestScope.rootCause.stackTrace) > 5}">
      ...
    </c:if>
    </p>
  </c:if>
</c:if>
</div>
<p>If this error persists, please submit a problem report to
<a href="http://4help.vt.edu">VT 4Help</a>.  Please include the following
server name in your 4Help problem report:
<strong>${requestScope.address.hostName}</strong>.</p>

<jsp:directive.include file="includes/error-bottom.jsp" />
