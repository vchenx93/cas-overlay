<jsp:directive.include file="includes/error-top.jsp" />

<div id="title">403 - Forbidden</div>
<div id="error">
  <p class="error">You are not authorized to access this resource.</p>
</div>

<jsp:directive.include file="includes/error-bottom.jsp" />
