<?xml version="1.0" encoding="UTF-8"?>
<beans xmlns="http://www.springframework.org/schema/beans"
       xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
       xmlns:c="http://www.springframework.org/schema/c"
       xmlns:context="http://www.springframework.org/schema/context"
       xmlns:mvc="http://www.springframework.org/schema/mvc"
       xmlns:p="http://www.springframework.org/schema/p"
       xmlns:webflow="http://www.springframework.org/schema/webflow-config"
       xsi:schemaLocation="
       http://www.springframework.org/schema/beans http://www.springframework.org/schema/beans/spring-beans-3.1.xsd
       http://www.springframework.org/schema/context http://www.springframework.org/schema/context/spring-context-3.1.xsd
       http://www.springframework.org/schema/mvc http://www.springframework.org/schema/mvc/spring-mvc-3.1.xsd
       http://www.springframework.org/schema/webflow-config http://www.springframework.org/schema/webflow-config/spring-webflow-config-2.3.xsd">
       
  <import resource="spring-configuration/propertyFileConfigurer.xml"/>

  <mvc:annotation-driven />

  <!-- Scan for annotated controllers -->
  <context:component-scan base-package="edu.vt.middleware.cas.web"/>

  <!-- Theme Resolver -->
  <bean id="themeResolver" class="org.jasig.cas.services.web.ServiceThemeResolver"
    p:defaultThemeName="${cas.themeResolver.defaultThemeName}"
    p:argumentExtractors-ref="argumentExtractors"
    p:servicesManager-ref="servicesManager">
    <property name="mobileBrowsers">
      <map>
        <entry key=".*iPhone.*" value="iphone" />
        <entry key=".*Android.*" value="iphone" />
        <entry key=".*Safari.*Pre.*" value="iphone" />
        <entry key=".*Nokia.*AppleWebKit.*" value="iphone" />
       </map>
    </property>
  </bean>
       
  <!-- View Resolver -->
  <bean id="viewResolver" class="org.springframework.web.servlet.view.ResourceBundleViewResolver"
    p:order="0">
    <property name="basenames">
      <list>
        <value>${cas.viewResolver.basename}</value>
        <value>protocol_views</value>
      </list>
    </property>
  </bean>
  
  <!-- Locale Resolver -->
  <bean id="localeResolver" class="org.springframework.web.servlet.i18n.CookieLocaleResolver" />
    
  <bean id="localeChangeInterceptor" class="org.springframework.web.servlet.i18n.LocaleChangeInterceptor" />
  
  <bean id="urlBasedViewResolver"
    class="org.springframework.web.servlet.view.UrlBasedViewResolver"
    p:viewClass="org.springframework.web.servlet.view.InternalResourceView"
    p:prefix="/WEB-INF/view/jsp/"
    p:suffix=".jsp" />

  <bean class="org.springframework.web.servlet.mvc.SimpleControllerHandlerAdapter" />

  <bean id="errorHandlerResolver"
    class="org.springframework.web.servlet.handler.SimpleMappingExceptionResolver"
    p:defaultErrorView="error-spring"
    p:warnLogCategory="org.springframework.web.servlet">
    <property name="exceptionMappings">
      <map>
        <entry key="NoSuchFlowExecutionException" value="casLoginTimeoutView" />
      </map>
    </property>
  </bean>

  <bean class="org.springframework.web.servlet.mvc.method.annotation.RequestMappingHandlerMapping"
      p:alwaysUseFullPath="true"/>

  <bean id="handlerMappingC"
        class="org.springframework.web.servlet.handler.SimpleUrlHandlerMapping"
        p:alwaysUseFullPath="true">
    <property
      name="mappings">
      <props>
        <prop key="/logout">logoutController</prop>
        <prop key="/serviceValidate">serviceValidateController</prop>
        <prop key="/validate">legacyValidateController</prop>
        <prop key="/proxy">proxyController</prop>
        <prop key="/proxyValidate">proxyValidateController</prop>
        <prop key="/samlValidate">samlValidateController</prop>
        <prop key="/secure/">secureIndexViewController</prop>
        <prop key="/secure/loggedOut.html">secureLogoutViewController</prop>
        <prop key="/openid/*">openIdProviderController</prop>
        <prop key="/warn">warnController</prop>
        <prop key="/status">healthCheckController</prop>
        <prop key="/help">helpController</prop>
        <prop key="/bannerValidate">bannerAccountValidateController</prop>
      </props>
    </property>
    <!--
    uncomment this to enable sending PageRequest events. 
    <property
      name="interceptors">
      <list>
        <ref bean="pageRequestHandlerInterceptorAdapter" />
      </list>
    </property>
     -->
  </bean>
  
  <bean
    id="openIdProviderController"
    class="org.jasig.cas.web.OpenIdProviderController"
    p:loginUrl="${cas.securityContext.casProcessingFilterEntryPoint.loginUrl}" />
  
  <bean
    id="secureIndexViewController"
    class="org.springframework.web.servlet.mvc.ParameterizableViewController"
    p:viewName="secureIndexView" />

  <bean
      id="secureLogoutViewController"
      class="org.springframework.web.servlet.mvc.ParameterizableViewController"
      p:viewName="secureLoggedOutView" />

  <bean class="org.springframework.webflow.mvc.servlet.FlowHandlerMapping"
    p:flowRegistry-ref="flowRegistry" p:order="2">
    <property name="interceptors">
      <ref local="localeChangeInterceptor" />
    </property>
  </bean>

  <bean class="org.springframework.webflow.mvc.servlet.FlowHandlerAdapter"
    p:flowExecutor-ref="flowExecutor" />

  <!--
    Define custom FlowHandler that lets NoSuchFlowExecutionExceptions through
    in support of our custom "login session expired" page.
    -->
  <bean name="login" class="edu.vt.middleware.cas.web.flow.NoSuchFlowHandler" />

  <bean name="flowExecutor" class="org.springframework.webflow.executor.FlowExecutorImpl">
    <constructor-arg ref="flowRegistry" />
    <constructor-arg ref="flowExecutionFactory" />
    <constructor-arg ref="flowExecutionRepository" />
  </bean>

  <webflow:flow-registry id="flowRegistry" flow-builder-services="builder">
    <webflow:flow-location path="/WEB-INF/login-webflow.xml" id="login" />
    <webflow:flow-location path="/WEB-INF/forgotten-credentials-webflow.xml" id="iforgot" />
  </webflow:flow-registry>

  <webflow:flow-builder-services id="builder" view-factory-creator="viewFactoryCreator" />

  <bean name="flowExecutionFactory" class="org.springframework.webflow.engine.impl.FlowExecutionImplFactory"
        p:executionKeyFactory-ref="flowExecutionRepository" />

  <bean id="flowExecutionRepository" class="edu.vt.middleware.webflow.ClientFlowExecutionRepository">
    <constructor-arg ref="flowExecutionFactory" />
    <constructor-arg ref="flowRegistry" />
    <constructor-arg ref="flowTranscoder" />
  </bean>

  <bean id="viewFactoryCreator" class="org.springframework.webflow.mvc.builder.MvcViewFactoryCreator">
    <property name="viewResolvers">
      <list>
        <ref local="viewResolver"/>
      </list>
    </property>
  </bean>

  <bean id="flowTranscoder" class="edu.vt.middleware.webflow.EncryptedTranscoder"
        p:compression="true">
    <property name="cipherBean">
      <bean class="org.cryptacular.bean.AEADBlockCipherBean"
            p:keyAlias="${webflow.client.encryption.keyAlias}"
            p:keyPassword="${webflow.client.encryption.keyPass}">
        <property name="keyStore">
          <bean factory-bean="keystoreFactory" factory-method="newInstance" />
        </property>
        <property name="blockCipherSpec">
          <bean class="org.cryptacular.spec.AEADBlockCipherSpec"
                c:algName="AES"
                c:cipherMode="GCM"/>
        </property>
        <property name="nonce">
          <bean class="org.cryptacular.generator.sp80038d.RBGNonce" />
        </property>
      </bean>
    </property>
  </bean>

  <bean id="keystoreFactory" class="org.cryptacular.bean.KeyStoreFactoryBean"
        p:type="${webflow.client.encryption.keyStoreType}"
        p:password="${webflow.client.encryption.keyStorePass}">
    <property name="resource">
      <bean class="org.cryptacular.io.FileResource"
            c:file="${webflow.client.encryption.keyStorePath}" />
    </property>
  </bean>

  <bean id="proxyValidateController" class="org.jasig.cas.web.ServiceValidateController"
    p:centralAuthenticationService-ref="centralAuthenticationService"
    p:proxyHandler-ref="proxy20Handler"
    p:argumentExtractor-ref="casArgumentExtractor" />

  <bean id="serviceValidateController" class="org.jasig.cas.web.ServiceValidateController"
    p:validationSpecificationClass="org.jasig.cas.validation.Cas20WithoutProxyingValidationSpecification"
    p:centralAuthenticationService-ref="centralAuthenticationService"
    p:proxyHandler-ref="proxy20Handler"
    p:argumentExtractor-ref="casArgumentExtractor" />
  
  <bean id="samlValidateController" class="org.jasig.cas.web.ServiceValidateController"
    p:validationSpecificationClass="org.jasig.cas.validation.Cas20WithoutProxyingValidationSpecification"
    p:centralAuthenticationService-ref="centralAuthenticationService"
    p:proxyHandler-ref="proxy20Handler"
    p:argumentExtractor-ref="samlArgumentExtractor"
    p:successView="casSamlServiceSuccessView"
    p:failureView="casSamlServiceFailureView" />

  <bean id="legacyValidateController" class="org.jasig.cas.web.ServiceValidateController"
    p:proxyHandler-ref="proxy10Handler"
    p:successView="cas1ServiceSuccessView"
    p:failureView="cas1ServiceFailureView"
    p:validationSpecificationClass="org.jasig.cas.validation.Cas10ProtocolValidationSpecification"
    p:centralAuthenticationService-ref="centralAuthenticationService"
    p:argumentExtractor-ref="casArgumentExtractor" />

  <bean id="proxyController" class="org.jasig.cas.web.ProxyController"
    p:centralAuthenticationService-ref="centralAuthenticationService" />

  <bean id="logoutController" class="org.jasig.cas.web.LogoutController"
    p:centralAuthenticationService-ref="centralAuthenticationService"
    p:logoutView="casLogoutView"
    p:warnCookieGenerator-ref="warnCookieGenerator"
    p:ticketGrantingTicketCookieGenerator-ref="ticketGrantingTicketCookieGenerator" />

  <bean id="healthCheckController" class="org.jasig.cas.web.HealthCheckController"
    p:healthCheckMonitor-ref="healthCheckMonitor"/>
  
  <bean id="initialFlowSetupAction" class="org.jasig.cas.web.flow.InitialFlowSetupAction"
    p:argumentExtractors-ref="argumentExtractors"
    p:warnCookieGenerator-ref="warnCookieGenerator"
    p:ticketGrantingTicketCookieGenerator-ref="ticketGrantingTicketCookieGenerator" />
  
  <bean id="authenticationViaFormAction" class="org.jasig.cas.web.flow.AuthenticationViaFormAction"
    p:centralAuthenticationService-ref="centralAuthenticationService"
    p:warnCookieGenerator-ref="warnCookieGenerator" />
 
  <!-- Needed for handling warn cookies for non-interactive authentication, e.g. PDC --> 
  <bean id="warnCookieHandlerAction" class="edu.vt.middleware.cas.web.flow.WarnCookieHandlerAction"
    p:warnCookieGenerator-ref="warnCookieGenerator" />
  
  <bean id="generateServiceTicketAction" class="org.jasig.cas.web.flow.GenerateServiceTicketAction"
    p:centralAuthenticationService-ref="centralAuthenticationService" />
    
  <bean id="sendTicketGrantingTicketAction" class="org.jasig.cas.web.flow.SendTicketGrantingTicketAction"
    p:centralAuthenticationService-ref="centralAuthenticationService"
    p:ticketGrantingTicketCookieGenerator-ref="ticketGrantingTicketCookieGenerator" />

  <bean id="gatewayServicesManagementCheck" class="org.jasig.cas.web.flow.GatewayServicesManagementCheck">
    <constructor-arg index="0" ref="servicesManager"/>
  </bean>

  <bean id="lookupRegisteredServiceAction" class="edu.vt.middleware.cas.web.flow.LookupRegisteredServiceAction">
    <constructor-arg index="0" ref="servicesManager"/>
  </bean>
        
  <bean id="generateLoginTicketAction" class="org.jasig.cas.web.flow.GenerateLoginTicketAction"
    p:ticketIdGenerator-ref="loginTicketUniqueIdGenerator" />

  <bean id="edLdapTemplate" class="org.springframework.ldap.core.LdapTemplate">
    <constructor-arg><ref bean="pooledEdIdContextSource" /></constructor-arg>
  </bean>

  <bean id="abstractLdapAction" abstract="true"
    p:ldapTemplate-ref="edLdapTemplate" />

  <!-- CAS-101: Password change workflow support -->
  <bean id="checkPasswordExpirationAction" parent="abstractLdapAction"
    class="edu.vt.middleware.cas.web.flow.CheckPasswordExpirationAction"
    p:passwordChangeUrl="${password.change.url}"
    p:guestPasswordChangeUrl="${password.change.guest.url}" />

  <!-- CAS-102: Password recovery management workflow support -->
  <bean id="checkAccountRecoveryMaintenanceAction" parent="abstractLdapAction"
    class="edu.vt.middleware.cas.web.flow.CheckAccountRecoveryMaintenanceAction"
    p:centralAuthenticationService-ref="centralAuthenticationService"
    p:ticketGrantingTicketCookieGenerator-ref="ticketGrantingTicketCookieGenerator"
    p:accountRecoveryManagementSetUrl="${account.recovery.set.url}"
    p:accountRecoveryManagementVerifyUrl="${account.recovery.verify.url}"
    p:maxInactivityPeriod="${account.recovery.maxInactivityPeriod}" />

  <!-- CAS-141: Password expiration warnings -->
  <bean id="checkImpendingPasswordExpirationAction" parent="abstractLdapAction"
        class="edu.vt.middleware.cas.web.flow.CheckImpendingPasswordExpirationAction"
        p:centralAuthenticationService-ref="centralAuthenticationService"
        p:ticketGrantingTicketCookieGenerator-ref="ticketGrantingTicketCookieGenerator"
        p:passwordChangeUrl="${password.change.url}"
        p:guestPasswordChangeUrl="${password.change.guest.url}"
        p:warningPeriod="30" />

  <!-- Forgotten credential webflow actions -->
  <bean id="forgotTypeAction"
    class="edu.vt.middleware.cas.web.flow.ForgottenCredentialTypeAction" />
  <bean id="affiliationAction"
    class="edu.vt.middleware.cas.web.flow.AffiliationHandlerAction" />
  <bean id="userLookupAction"
    class="edu.vt.middleware.cas.web.flow.UserLookupAction"
    p:edIdContextSource-ref="pooledEdIdContextSource"
    p:edLiteContextSource-ref="pooledEdLiteContextSource"
    p:passwordResetUrl="${account.recovery.url}"
    p:guestPasswordResetUrl="${password.change.guest.url}" />

  <bean id="registeredServiceController" class="edu.vt.middleware.cas.web.RegisteredServiceController"
      p:listView="serviceListingView"
      p:formView="serviceFormView"
      p:duplicateView="serviceDuplicateView"
      p:personAttributeDao-ref="attributeRepository"
      p:propertyComparator-ref="registeredServiceListingComparator" />

  <bean id="registeredServiceValidator" class="org.jasig.cas.services.web.support.RegisteredServiceValidator"
    p:servicesManager-ref="servicesManager"
    p:maxDescriptionLength="1000" />

  <bean id="messageInterpolator" class="org.jasig.cas.util.SpringAwareMessageMessageInterpolator" />

  <bean id="credentialsValidator" class="org.springframework.validation.beanvalidation.LocalValidatorFactoryBean"
           p:messageInterpolator-ref="messageInterpolator" />
  
  <bean id="warnController"
    class="edu.vt.middleware.cas.web.WarnController"
    p:viewName="casLoginConfirmView"
    p:loginUrl="/login" />

  <!-- Supports authentication with X.509 client certificate -->
  <bean id="x509Check"
    p:centralAuthenticationService-ref="centralAuthenticationService"
    class="org.jasig.cas.adaptors.x509.web.flow.X509CertificateCredentialsNonInteractiveAction" />

  <bean id="helpController"
    class="edu.vt.middleware.cas.web.HelpController"
    p:viewName="casHelpView"
    p:accountRecoveryUrl="${account.recovery.url}"
    p:accountRecoveryFaqUrl="${account.recovery.faq.url}"
    p:accountRecoveryManagementUrl="${account.recovery.set.url}" />
  
  <!-- BEIS integration -->  
  <bean id="bannerAccountValidateController"
    class="org.jasig.cas.web.ServiceValidateController"
    p:validationSpecificationClass="org.jasig.cas.validation.Cas20WithoutProxyingValidationSpecification"
    p:centralAuthenticationService-ref="centralAuthenticationService"
    p:proxyHandler-ref="proxy20Handler"
    p:argumentExtractor-ref="bannerArgumentExtractor"
    p:successView="bannerAccountServiceSuccessView"
    p:failureView="bannerAccountServiceFailureView" />

  <!-- Next two Webflow actions backported from CAS 4.0. -->
  <bean id="ticketGrantingTicketCheckAction"
        class="edu.vt.middleware.cas.web.flow.TicketGrantingTicketCheckAction"
        c:registry-ref="ticketRegistry" />

  <bean id="terminateSessionAction"
        class="edu.vt.middleware.cas.web.flow.TerminateSessionAction"
        c:cas-ref="centralAuthenticationService"
        c:tgtCookieGenerator-ref="ticketGrantingTicketCookieGenerator"
        c:warnCookieGenerator-ref="warnCookieGenerator"/>
    
</beans>
