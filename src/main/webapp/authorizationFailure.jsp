<jsp:directive.include file="/WEB-INF/view/jsp/includes/error-top.jsp" />

<h1>Authorization Failed</h1>
<div id="error">
  <p class="error">You are not authorized to access this resource.</p>
</div>

<jsp:directive.include file="/WEB-INF/view/jsp/includes/error-bottom.jsp" />
