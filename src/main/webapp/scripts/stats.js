// Plots a data series in flot JSON format in the given container to hold the
// plot and legend, respectively.
function plotSeries(jsonUrl, plot_container, legend_container) {
  var options = {
    series: {
      lines: { show: true, fill: false, lineWidth: 1}
    },
    xaxis: { mode: "time", twelveHourClock: false },
    legend: {
      backgroundOpacity: 0,
      noColumns: 2,
      container: legend_container,
    }
  };

  $.getJSON(jsonUrl, function (json) {
    $.plot(plot_container, json, options);
  });
}


// Fills a table with rows and columns from a JSON array of arrays.
// The headers parameter should be an array of header names.
function fillTable(jsonUrl, table, headers) {
  $.getJSON(jsonUrl, function (json) {
    var rows = new Array(), count = -1;

    rows[++count] = '<tr>';
    for (var i = 0; i < headers.length; i++){
      rows[++count] = '<th>';
      rows[++count] = headers[i];
      rows[++count] = '</th>';
    }
    rows[++count] = '</tr>';

    for (var i = 0; i < json.length; i++){
      rows[++count] = '<tr>';
      for (var j = 0; j < json[i].length; j++) {
        rows[++count] = '<td>';
        rows[++count] = json[i][j];
        rows[++count] = '</td>';
      }
      rows[++count] = '</tr>';
    }
    table.html(rows.join(''));
  });
}